//--------------------
// SNRF240l code ici
//--------------------
#define numeroNXP 0 // 1 en reception, 0 en transmission, comme pour la fonction plus bas
#include <stdio.h>
#include "LPC8xx.h"
#include "spi.h"
#include "syscon.h"
#include "swm.h"
#include "utilities.h"

#include "lib_ENS_II1_lcd_v2.h"


//extern void setup_debug_uart(void);

#define SPIBAUD 9600 // à cette fréquence pas de soucis de timing paussible
// peut être augmenter pour permettre des vitesses de transferts plus grandes
// à vos risques et périls
#define SCK_PIN P0_19
#define MOSI_PIN P0_18
#define MISO_PIN P0_13
#define SSEL_PIN P0_21
#define loupiotte_Rouge_temoins P0_20
// CE défini une sortie
#define CE_PIN P0_11 // pin de CE nécéssaire pour l’envois / potentiellement nécéssaire
#define IRQ_PIN P0_12 // à choisir
// IRQ est une entrée

//
#define taille_adresse_default 5
#define taille_message_default 32 // taille max des messages de toute manière
#include "mesFCT_NRF240l.h"

// variable à ajouter à la structurs de NRF24
int nouveau_MSG;


void CE(int mode){
	/*
	 *remis en l’état normalement pendant le reset
	LPC_GPIO_PORT->DIR0 |= (1 << CE_PIN);
	*/


	if(mode){
	LPC_GPIO_PORT->B0[CE_PIN] = 1; // mise à l’état haut du port CE
	} else {
	LPC_GPIO_PORT->B0[CE_PIN] = 0; // mise à l’état haut du port CE
	}
}


int IRQ(){
	return LPC_GPIO_PORT->B0[IRQ_PIN] ^1; // Interruption à l’état haut
}

void setUpRecepteur(uint8_t numeroRecepteur,uint8_t adresse[],uint8_t tailleMessage){
	uint8_t registre;
	// on prendra par défault des adresses sur 5 bytes

	Ecrire_un_registre8bit(NRF24_SETUP_AW, NRF24_TailleAdresse5octets); // on écrit dans le registre
	attend(); // il faut laisser un peu de temps au circuit pour prendre tout en compte

	if(numeroRecepteur < 2){
	Ecrire_un_registreNbyte(NRF24_RX_ADDR_P[numeroRecepteur],adresse, 5); // seul les adresses
	// des canaux 0 et 1  sont entièrement customizable.Pour les autres adresses hérites des n-1 premiers bytes de l’adresse
	// sur le canals 1. Avec n la taille de l’adresse
	} else {
		Ecrire_un_registre8bit(NRF24_RX_ADDR_P[numeroRecepteur],adresse[4]); // il faudrait prendre en compte le fait que les adresses soit de talles
		// dynamique, mais c’est peut contraignent d’avoir des adresses de tailles fix
	}


	Ecrire_un_registre8bit(NRF24_RX_PW_P[numeroRecepteur], tailleMessage); // taille des messages

	// paramètrage de l’ack automatique
	Lire_un_registre8bit(NRF24_EN_AA, &registre);
	registre |= (1 << numeroRecepteur); // permet d’allumer l’ack auto sur le data pipe 0
	Ecrire_un_registre8bit(NRF24_EN_AA, registre);

	// activation de de l’adresse de reception
	// elle sont par défault désactivé (sauf pour la première et la seconde) donc il faut les activés
	Lire_un_registre8bit(NRF24_EN_RXADDR, &registre);
	registre |= (1<< numeroRecepteur);
	Ecrire_un_registre8bit(NRF24_EN_RXADDR, registre); // active les bons canaux pour la receptions du messages


}


void reEcoute(){
	CE(1);// pour lancer la phase d’écoute active
	// il faut maintenir le pin CE à 1
}

void stopEcoute(){// ces fonctions ne servent à rien
	// mais leurs nom indique le utilitées
	CE(0);
}

void Commence_Ecoute_adresse(uint8_t adresse[]){
	// À l’avenir, c’est cette fonctions qui gérera dynamiquement les adresses
	// et l’espace dans le recepteurs fifo
	int tailleMessage = 2;
	int numeroRecepteur = 0;
	setUpRecepteur(numeroRecepteur, adresse, tailleMessage);
	reEcoute();

}

int recup_Message(uint8_t messageComplet){
	int nouvMSG;
	demande_status();
	nouvMSG = NRF24.status & (1 << NRF24_RX_DR);// on chèque si il y a un message disponible
	if( nouvMSG){
			Lire_un_registreNbyte(NRF24_R_RX_PAYLOAD, messageComplet, 32);
			Ecrire_un_registre8bit(NRF24_STATUS, NRF24.status); // on abaisse le drapeau
			return 1;

	}
	return 0;


}



void OnOff(int mode){
	// Par default RX
	uint8_t Eteindre =  0b01;
	uint8_t Allumer = 0b11;
	uint8_t state;
	Lire_un_registre8bit(NRF24_CONFIG,&state);
	state &= ~(Allumer); // on masque l’état précédent
	if(mode){
		Ecrire_un_registre8bit(NRF24_CONFIG, state | Allumer);
	} else {
		Ecrire_un_registre8bit(NRF24_CONFIG, state | Eteindre);
	}
}

void RX1TX0(int mode){
	attend();
	uint8_t state;
	Lire_un_registre8bit(NRF24_CONFIG,&state);// lecture de l'état du registre de configuration
	state &= ~(1<<NRF24_CRCO); // impose la valeur par défault tant qu’à faire
	if(mode){
		// Activation mode RX
		state |= (1 << NRF24_PRIM_RX) | (1 << NRF24_PWR_UP) | (1 << NRF24_EN_CRC); // Power Up permet d'allumer la carte
	} else { // mode 0
		// Activation mode TX
		state &= ~(1 << NRF24_PRIM_RX); //
		state |= 1 << NRF24_PWR_UP; // Power_UP
	}
	// on écrit l’état en fonction de la situation
	Ecrire_un_registre8bit(NRF24_CONFIG,state);


}

void reinitialise(){ // semble parfaitement inutile à prioris
	// puisque pour reset le composant il faut lui couper le jus
	RX1TX0(1);
	OnOff(0);
	attend();
	OnOff(1);
}





void Transmet_msg(uint8_t message[], uint8_t nbBytemsg, uint8_t adresse[]){

	CE(0);// on éteint la chip pour la phase de paramètrage
	OnOff(0); // mode standby éteint
	// RX1TX0(1); déjà le cas en après OnOff(0)

    // paramètrage de l'automate d'envois
	uint8_t state;
	Lire_un_registre8bit(NRF24_EN_RXADDR,&state);
	state |= (1<<NRF24_ENAA_P0); // nécessaire pour la réception automatique de l'ACK
	state |= (1<<NRF24_ENAA_P1);
	attend();
	Ecrire_un_registre8bit(NRF24_EN_RXADDR,state);

	// envois du message lui même
	Ecrire_un_registreNbyte(NRF24_TX_ADDR,adresse, taille_adresse_default); // ecriture du buffert de reception
	attend();
	Ecrire_un_registreNbyte(NRF24_RX_ADDR_P0,adresse, taille_adresse_default); // nécessaire pour la réception de l'ACK
	attend();
	OnOff(1); // on allume l'appareil (en RX évidemment)
	attend();
	Ecrire_un_registreNbyte(NRF24_W_TX_PAYLOAD, message, nbBytemsg);// ecriture du message
	attend();
	RX1TX0(0); // on envois le message le reste est généré automatiquement
	//	attend();
//	RX1TX0(1); // le mode réception est le mode par défault pour nous
//	attend();

	attend();

	CE(1);
	int irqOld;
	for(int i = 0;i<20;i++){
		attend(); // le gestion de l’envois est gérér d’ici

		if((IRQ() != 0) & (irqOld !=0)){
			break;
		}
		irqOld = IRQ();
	}
	// l'impulsion sur le pin CE doit être assez long pour être détecté par la machine
	// environ 10 microsecondes
	CE(0);
	}


int different(uint8_t * adresse1,uint8_t * adresse2,int n){
	// test permettant de vérifié la bonne communication avec la cartes
	for(int i = 0; i<n;i++){
		if(*(adresse1+i)!=*(adresse2+i)) return 1; // vaut 1 quand ils sont différents
		// cela permet de voir qu’il y a bien un problème de communication avec la puce
	}

	return 0; // pas de soucis de communication avec le module

}

int differentRec(uint8_t * adresse1,uint8_t * adresse2, int n){ // version recursive
	if(!n) return 1;
	return ((*adresse1 == *adresse2) & differentRec(adresse1+1,adresse2+1,n-1)); // parce que cela est plus drole et que cela ce fait biens
}


void preset(){ // rénitialise la puce en état d’envois et ou de réception

	LPC_GPIO_PORT->DIR0 |= (1 << CE_PIN) | (1 << loupiotte_Rouge_temoins);


	LPC_GPIO_PORT->DIRCLR0 = ~0; // on force remise à zeros de l’état des ports du
	// GPIO à chaque redémarrage

	SPI_setup();
	attend(); // problème après la configuration des ports en sortie

    CE(0);
	// /!\ Il peut y avouir de problème entre SPI_setup et la gestion des ports
	//Ecrire_un_registre8bit(NRF24_FLUSH_TX,0);
	attend();

	OnOff(0);

	uint8_t adresseDeTest[5] = {1,2,3,4,5};
	uint8_t adresseDeTest0[5] = {0,0,0,0,0};
	Ecrire_un_registreNbyte(NRF24_RX_ADDR_P0,adresseDeTest, 5);// très donc à revoir
	Lire_un_registreNbyte(NRF24_RX_ADDR_P0, adresseDeTest0, 5);
	Ecrire_un_registre8bit(NRF24_RF_SETUP, (1<<5));
	int i = 0;

	// une
	while(different(adresseDeTest,adresseDeTest0,5)){
		LPC_GPIO_PORT->NOT0 |= (1 << loupiotte_Rouge_temoins); // En cas de problème de connectique
		// on fait clignoté la led rouge

		for(i = 0;i<20;i++){ // on mettra ne place une vrai temporisation plus tard
			adresseDeTest[i%5] += i; // on change la valeurs de tests
			attend();
           }
		Ecrire_un_registreNbyte(NRF24_RX_ADDR_P0,adresseDeTest, 5);
		Lire_un_registreNbyte(NRF24_RX_ADDR_P0, adresseDeTest0, 5);

	}


	SPI_envoi(NRF24_FLUSH_TX, 1);// on reset les registres fifo
	attend();
	SPI_envoi(NRF24_FLUSH_RX, 1);//

	demande_status(); // met à jours le status de la NRF24
	Ecrire_un_registre8bit(NRF24_STATUS, NRF24.status); // permet d’abaisser
	// les flags

	Ecrire_un_registre8bit(NRF24_RF_CH,115);
	// on se place à 2.515GHz

 	Ecrire_un_registre8bit(NRF24_SETUP_RETR, 15);
	// nombre de renvois automatique sur échec
	attend();
	Ecrire_un_registre8bit(NRF24_EN_RXADDR,0b11);
	// active les adresses 0 et 1 à la réception

	attend();
	Ecrire_un_registre8bit(NRF24_EN_AA,0x3F); // active l'ack auto
	attend();
	Ecrire_un_registre8bit(NRF24_SETUP_AW,0b11); // reglage à 5 la taille des adresses

}



int main(void) {
	int envois = 0;
	preset();
	int succes = 0;
 	uint8_t adresse0 [5] = {0x1,0x2,0x3,0x4,0x5}; // une adresse est une liste de bytes
	uint8_t text0 [32] = "coucou c’est un succès";
	char affichage[32];
	char messageComplet[33] = "    nothing";// on met le fin de caractère
	uint8_t observeTX;

	init_lcd();
	if(envois) lcd_puts("projet TX");
	else lcd_puts("projet RX");


	if(envois) Transmet_msg(text0, 32 ,adresse0);// adresse de celui qui
	// message envoy une seule fois par reset
	//demande_status();
	succes = NRF24.status & (1 << 5); // bit de succès d’envois


	if(!envois) Commence_Ecoute_adresse(adresse0);

	while (1) {
		messageComplet[0] = 48+IRQ();
		messageComplet[2] = 48 + NRF24.status & 0xF;
		messageComplet[3] = 48 + (NRF24.status >> 4) & 0xF;
		if(!envois){
		if(recup_Message(messageComplet[33]) ){
			messageComplet[32] = '\0';
			while(1);
		}
		} else {

			Lire_un_registre8bit(NRF24_OBSERVE_TX, &observeTX);
		}
		attend();
		lcd_position(1,0);
		demande_status();

		if(envois) sprintf(messageComplet,"%x %x",NRF24.status,observeTX);

		lcd_puts(messageComplet);




	}

}  // end of main
