/*
Macro des registres et des commandes, avec les numéros des bits à modifier 
avec des indications sur comment les modifiés si nécéssaire.
Fait entre 23h00 et 2h00, peut contenire des fautes d’orthographes.
Mais le registre et commande fûrent tapper avec beaucoup de soin.


#TODO : vérifier les registre 
#TODO : vérifier l’orthographe et la pertinance de commentaire
#TODO : retravailler la nomenclature
*/


/*
commande à envoyer sur le port MISO, voir table 19 page 48 de la datasheet du composant.
 
*/

#define AAAAA 00000 // Les addresses mémoires de la mémory map sont écrites dans cette partie du bit
#define PPP 000

#define NRF24_R_REGISTER         0b00000000 // lecture des registres mémoires et du registre de status
#define NRF24_W_REGISTER         0b00100000 // commande écriture des registres mémoires
// AAAAA = addresse du registre mémoire dans la mémoire 
#define NRF24_R_RX_PAYLOAD        0b01100001 // commande lectures des messages reçus
#define NRF24_W_TX_PAYLOAD        0b10100000 // commande écritures des messages
#define NRF24_FLUSH_TX            0b11100001 // flush TX FIFO 
#define NRF24_FLUSH_RX            0b11100010 // flush RX FIFO
#define NRF24_REUSE_TX_PL         0b11100011 // renvois le même messages
#define NRF24_R_RX_PL_WID         0b01100000 // lit le registre de réception 
#define NRF24_W_ACK_PAYLOAD       0b10101PPP 
#define NRF24_W_TX_PAYLOAD_NO_ACK 0b10110000 // désactive l’autoACK (auto acknowledgement)
#define NRF24_NOP                 0b11111111 // No OPeration, permet de lire le registre d’état


// certaines commandes sont suivis d’une séquence d’envois de bytes. 
// Cela sera géré par les fonctions que l’on écrira dans un temps certains


/* 
Ici sont créées les macros des positions des bits de commande dans les 
différent registres. On commance par écrire dans la commande
R_REGISTER puis on luis ajoute l’addresse EXA reservé pour le registres.
Le bytes de commande arrivant après sera celui de configuration.

On gardera sur la LPC une structure miroire que l’on modifiera.


On pourra noter qu’écrire la macro ne sert rien R_REGISTER, en effet pour
écrire dans un registre il suffit d’écrire son adresse comme bytes de commande.
Mais c’est bien de l’écrire quelque part.


La commmande NOP est bien si l’on veut test le bon fonctionnement de la LPC et de la communication avec la carte.
*/

// 1ère adresse d’écriture
#define NRF24_CONFIG              0x00 // addresse des registres de configuration 

// Mask d’interruption, sur réception d’un message le pin IRQ change d’état
#define NRF24_MASK_RX_DR          6 
#define NRF24_MASK_TX_DS          5 

// 
#define NRF24_MASK_MAX_RT         4
#define NRF24_EN_CRC              3
#define NRF24_CRCO                2
#define NRF24_PWR_UP              1  // permet d’éteindre la puce
#define NRF24_PRIM_RX             0  // controle le passage des modes RX,TX . 1:PRX , 0:PTX


// 2nd adresse, voir page 72

/* 
Enhanced ShockBurst, permet de réglé la distruibution automatique des 
acknowledgements.

*/

#define NRF24_EN_AA 0x01
// bit 6:7 reservé, toujours écrire 0 dessus
// registre d’activation des acknowledgements piplines, activer par défaut 
// le registre prend la valeurs par défault du registre 0b00111111
#define NRF24_ENAA_P5             5
#define NRF24_ENAA_P4             4
#define NRF24_ENAA_P3             3
#define NRF24_ENAA_P2             2
#define NRF24_ENAA_P1             1
#define NRF24_ENAA_P0             0

/*
C’est le registre de paramètr 
active la reception des messages des destinataires à l’adresses associés aux pipes.

Ils sont activées par défault donc ne pas y toucher est la meilleur chose à faire.
 */

#define NRF24_EN_RXADDR          0x02 

// bit 6:7 reservés, toujours écrire 0 dessus
// 

#define NRF24_ERX_P5             5
#define NRF24_ERX_P4             4
#define NRF24_ERX_P3             3
#define NRF24_ERX_P2             2
#define NRF24_ERX_P1             1
#define NRF24_ERX_P0             0


/*
Paramètrages de la taille des adresses associées aux pipelines

Extrait de la doc technique :
RX/TX Address field width
'00' - Illegal
'01' - 3 bytes
'10' - 4 bytes
'11' – 5 bytes
LSByte is used if address width is below 5 bytes

La valeur par défault est TailleAdresse5octets, les adresses en communications
donc pas de possible.

les bits 7:2 sont réservés, ne pas écrire ou écrire des 0 dessus
écrire 0b00000000 sur le registre est illégale (donc à ne surtout pas faires). La fonctions permettant de modifier cette variables n’est pas se verra associé une sécurité pour évité que cela ne se produise.
*/


#define NRF24_SETUP_AW           0x03

#define NRF24_TailleAdresse3octets 0b00000001
#define NRF24_TailleAdresse4octets 0b00000010
#define NRF24_TailleAdresse5octets 0b00000011

/* 
Paramètrage de la retransmission du renvois automatique en cas de problème
à l’envoie

ARD définie le delay entre les 2 envois :
Auto Retransmit Delay
‘0000’ – Wait 250µS
‘0001’ – Wait 500µS
‘0010’ – Wait 750µS
.......
‘1111’ – Wait 4000µS -> de facto la valeurs maximal entre les transmissions

Delais_entre_transmission = ARD * 250 microsecondes
Pas besoin de macros mais, 4 bits sont réservé pour cette valeurs.  

ARC définie le nombre de tentative de renvois, varie entre :
0 (  pas de nouvelles tentatives sur échec de l’envois) à 15.
4 bits sont réservé pour cette adresse.

’0000’ pas nouvelle tentative
’0001’ 1 tentative
’0010’ 2 tentatives 
......
’1111’ 15 tentatives 


Exemple d’écritures d’une commande (7 tentatives, 750µS de délais entre les transmission ) : 
R_REGISTER+SETUP_RETR >> MOSI  on écrit la commande pour écrire sur MOSI
0b0010 0111           >> MOSI  4 bits MSB pour ARD, les 4 suivants pour ARC
*/

#define NRF24_SETUP_RETR 0x04 

/* 
####### RF Channel

Définie les fréquence de communication de la puce, .
La doc est lacunaire sur ce point.

Le MSBit est interdit est resvervé. N’écrire que 0 dessus.
La valeur des bits 6:0 par défault est : 0000010 
Il est important que differente carte sur le même canal pour s’entendre
à prioris

*/

#define NRF24_RF_CH          0x05
#define NRF24_RF_CH_default  0b00000010 

/* 
RF Sutep Register 
Permet de manipulé la frequence de transmissino de la data.
bit 6 interdit, donc ne pas écrire autre chose que 0 dessus. 

RF_DR_HIGH et RF_DR_HIGH définisse la vitesse de transmission/reception :

[RF_DR_LOW, RF_DR_HIGH]:
‘00’ – 1Mbps
‘01’ – 2Mbps
‘10’ – 250kbps
‘11’ – Reserved


Donc ne pas écrire les deux bits à 1 . Par défault le couple vaut : 
’11’

RF_PWR définie la puissance de la Transmission : 
Set RF output power in TX mode
'00' – -18dBm
'01' – -12dBm
'10' – -6dBm
'11' – 0dBm    -> valeurs par default

On notera que par default la puce envois à la puissance maximum 

Le LSBit ne sert à rien, on peut écrire dessus mais cela ne changera rien.


*/


#define NRF24_RF_SETUP 0x06 


#define NRF24_CONT_WAVE        7 // Permet de maintenire le signal porteur en l’absence de transmission
#define NRF24_RF_DR_LOW        5  // impose la fréquence de transmission à 250kbps
#define NRF24_PRR_LOCK         4 // ne pas touchés, ne sert que pour les tests
#define NRF24_RF_DR_HIGH       3 // Permet de choisir la vitesse de communication max
#define NRF24_RF_PWR           1 // /!\ couple de 2 bits



/*
STATUS
Registre d’état, accessible à chaque transmission sur le port MOSI

le MSB (7) est réservé. 


Ces interruption doivent être traîter pour communiquer :

RX_DR interruption données reçue (plusieur bytes peuvent avoir été reçus)
TX_DS interruption sur envoie reception d’un Ack (AUTO_ACK)
MAX_RT maximum de TX retransmits atteint  (block toute transmission tant que bloqué à 1).


RX_P_NO numéro du data pipe pour la lecture sur RX_FIFO
reading from RX_FIFO
000-101: Data Pipe Number
110: Not Used
111: RX FIFO Empty
3 octets lui sont réservé. De toute façon tout sera géré dans la bibliothèque elle même.

TX_FULL la transmission est pleine ne plus rien envoyer please

*/

#define NRF24_STATUS    0x07 // renvois le status, uniquement le status
#define NRF24_RX_DR     6
#define NRF24_TX_DS     5
#define NRF24_MAX_RT    4
#define NRF24_TX_FULL   0 



// 7:4 PLOS_CNT, compte le nombre de paquet perdu,
// 3:0 ARC_CNT, compte du nombre de tentative de transmission d’un paquet
#define NRF24_OBSERVE_TX            0x08 



// 7:1 reservé, ne rien écrire dessus
#define NRF24_RPD                   0x09 // passe à un sur reception d’un signal porteur sur la signal le channel concérné


/* 

Adresse des pipes de receptions. Pour P0 et P1 39:0 peuvent être écrit
 librement. Mais pour P{1..5}, les bits 39:8 sont ceux de P1. on ne paramètre que le dernier byte (7:0).
*/
#define NRF24_RX_ADDR_P0            0x0a
#define NRF24_RX_ADDR_P1            0x0b
#define NRF24_RX_ADDR_P2            0x0c
#define NRF24_RX_ADDR_P3            0x0d
#define NRF24_RX_ADDR_P4            0x0e
#define NRF24_RX_ADDR_P5            0x0f

int NRF24_RX_ADDR_P[6] = {0xa,0xb,0xc,0xd,0xe,0xf};

/*
C’est l’adresse de transmission on dirait mais c’est plus compliqué que cela. Voir la doc au moment de l’écriture des programmes d’automatisation de la fonctions.
Transmit address. Used for a PTX device only.
(LSByte is written first)
Set RX_ADDR_P0 equal to this address to handle
automatic acknowledge if this is a PTX device with
Enhanced ShockBurst™ enabled. See page 72.

*/
#define NRF24_TX_ADDR               0x10


/*
Paramètrage de la taille des buffers de reception sur les différents pipes
0 si non utilisé, jusqu’à 32bytes.

Les bits 7:6 sont réservés. On écrit sur les bits 5:0 pour définir la taille du buffer.

#TODO : vérifier que la mémoire n’est pas partagé entre les buffers de receptions.

*/
#define NRF24_RX_PW_P0              0x11
#define NRF24_RX_PW_P1              0x12
#define NRF24_RX_PW_P2              0x13
#define NRF24_RX_PW_P3              0x14
#define NRF24_RX_PW_P4              0x15
#define NRF24_RX_PW_P5              0x16

int NRF24_RX_PW_P[6] = {0x11,0x12,0x13,0x14,0x15,0x16};



#define NRF24_FIFO_STATUS           0x17
#define NRF24_DYNPD                 0x1c


/* 
Il y a des trucs pratique à activer dedans donc faire gaffes.
*/
#define NRF24_FEATURE               0x1d






