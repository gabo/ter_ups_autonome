

// Il faudra faire le trie des bibliothèques
#include "ctimer.h"
#include "LPC8xx.h"
#include <stdio.h>
#include "syscon.h"
#include "spi.h"
#include "swm.h"
#include "utilities.h"
#include "rom_api.h"





#define SPIBAUD 115200 // une valeurs plus importante peut être choisi d’après la 
// documentation de la NRF24

// dans l’idéal le chois des pins ce fait lors d’un appel de fonction au moment
// de l’initialisation afin de s’adapter aux contraintes matérielles
#define SCK_PIN 19//P0_19
#define MOSI_PIN 18//P0_18
#define MISO_PIN 13//P0_13
#define SSEL_PIN 20//P0_21 // on règle le problbème ainsi
// /!\ Rien ne doit êter connecté
#define VraiSSEL_PIN 21// Port SSL piloté manuellement pour permettre l’envois 
// simultané de plusieurs bytes
#define CE_PIN 11//P0_11
/*start of low level functions, specific to the mcu and compiler*/

/*delay in miliseconds*/
void delay_function(uint32_t duration_ms){// on peut faire cela proprement
	LPC_CTIMER0->TCR = 1;
	LPC_CTIMER0->TC = 0;

	while(duration_ms*10 > LPC_CTIMER0->TC);
}

/*contains all SPI configuations, such as pins and control registers*/
/*SPI control: master, interrupts disabled, clock polarity low when idle, clock phase falling edge, clock up tp 1 MHz*/
void SPI_Initializer(){
	/*
	 * Recyclage de la fonctions SPI_setup()
	 */

	// Enable clocks to relevant peripherals
	LPC_PWRD_API->set_fro_frequency(30000);// on impose la fréquence de l’horloge à 15MHz

//	LPC_SYSCON->PRESETCTRL0 &= (1<<SPI0) | (1<<CTIMER0);
//	LPC_SYSCON->PRESETCTRL0 |= (1<<SPI0) | (1<<CTIMER0);
	LPC_SYSCON->SYSAHBCLKCTRL[0] |= (SPI0 | SWM);

	// Activation du périphérique d'entrées/sorties TOR
	LPC_SYSCON->SYSAHBCLKCTRL0 |= GPIO | CTIMER0; // À vérifier que cela n'entre pas en
	// conflits avec les communications SPI
	LPC_CTIMER0->PR = 1500-1 ; // on obtient une séparation à la 1/10 miliseconde
	LPC_CTIMER0->TC = 0;
	LPC_CTIMER0->TCR = 0;

	// Configure the SWM (see peripherals_lib and swm.h)
	ConfigSWM(SPI0_SCK, SCK_PIN);
	ConfigSWM(SPI0_MOSI, MOSI_PIN);
	ConfigSWM(SPI0_SSEL0, SSEL_PIN); // configuratio du chip select
	ConfigSWM(SPI0_MISO, MISO_PIN); // configuration d’un port MISO

	// Give SPI0 a reset
	LPC_SYSCON->PRESETCTRL[0] &= (SPI0_RST_N);
	LPC_SYSCON->PRESETCTRL[0] |= ~(SPI0_RST_N);

	// Enable main_clk as function clock to SPI
	LPC_SYSCON->SPI0CLKSEL = FCLKSEL_MAIN_CLK;

	// Get main_clk frequency
	SystemCoreClockUpdate();

	// Configure the SPI master's clock divider (value written to DIV divides by value+1)
	LPC_SPI0->DIV = (main_clk / SPIBAUD) - 1;

	// Configure the CFG register:
	// Enable=true, master, no LSB first, CPHA=0, CPOL=0, no loop-back, SSEL active low
	LPC_SPI0->CFG = SPI_CFG_ENABLE | SPI_CFG_MASTER;

	// Configure the SPI delay register (DLY)
	// Pre-delay = 0 clocks, post-delay = 0 clocks, frame-delay = 0 clocks, transfer-delay = 0 clocks
	LPC_SPI0->DLY = 0x0000;
	// Configure the SPI control register
	// Master: End-of-frame true, End-of-transfer true, RXIGNORE true, LEN 8 bits.
	LPC_SPI0->TXCTL = SPI_CTL_EOF | SPI_CTL_EOT | SPI_CTL_RXIGNORE
			| SPI_CTL_LEN(8);

	LPC_SPI0->TXCTL &= ~(0xF << 24);
	LPC_SPI0->TXCTL |= (0x7 << 24); // il faut écrire nb_bits - 1, 0 pour 1 bits, F pour 16 bit
}

/*contains all CSN and CE pins gpio configurations, including setting them as gpio outputs and turning SPI off and CE '1'*/
void pinout_Initializer()
{// pas nécessaire de précisé le port CSN
	LPC_GPIO_PORT->DIR0 |= (1 << CE_PIN) | (1 << 21 );// pin CE et le vrais ship select

}

/*CSN pin manipulation to high or low (SPI on or off)*/
void nrf24_SPI(uint8_t input)
{// On fait une commande manuelle du pin SSEL
	LPC_GPIO_PORT->B0[VraiSSEL_PIN] = input;// La commande 
}

/*1 byte SPI shift register send and receive routine*/
uint8_t SPI_send_command(uint8_t command)
{
		LPC_SPI0->TXCTL &= ~(1 << 22 ); // on retire le RXIGNORE
		while(!(LPC_SPI0->STAT & 2)); // tant que pas disponible à l’écriture pour la suite
		LPC_SPI0->TXDAT = command;

		uint8_t result;
		while (!(LPC_SPI0->STAT & 1)); // tant qu’aucune data n’à été ressus
		// sort de la boucle sur réception du flag de data reçus sur le ports du NRF24
		result = LPC_SPI0->RXDAT;
		return result; // on recycles les variables localements quand le code n’est pas très compliqué
}

/*CE pin maniplation to high or low*/
void nrf24_CE(uint8_t input){

//
//	if(input){ // ancien code
//	LPC_GPIO_PORT->B0[CE_PIN] = 1; // mise à l’état haut du port CE
//	} else {
//	LPC_GPIO_PORT->B0[CE_PIN] = 0; // mise à l’état haut du port CE
//	}
	LPC_GPIO_PORT->B0[CE_PIN] = input; // code plus court, moins de risque d’erreur

}
