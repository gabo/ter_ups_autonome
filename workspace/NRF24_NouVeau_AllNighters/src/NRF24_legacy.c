/*
 Le code et la librairie utilisé sont soit adapté ou totalement repris du code présent sur le git hub à la date du 15 janvier 1h32
 tout mes remerciement à leur auteur pour le travail qu’ils ont pu fournir



 */

// Programme de base TP II ENS
#include "lib_ENS_II1_lcd_v2.h"
#include "LPC8xx.h"
#include <stdio.h>
#include "syscon.h"
#include "nrf24l01.h"
#include "uart.h"
#include "swm.h"
#include "utilities.h"
#include "chip_setup.h"

#include "nrf24l01_low_level.h"

#define TIME_GAP    500
#define WaitForUART0txRdy  while(((LPC_USART0->STAT) & (1<<2)) == 0)
#define RX_BUFFER_SIZE 35


const unsigned char thestring[] = "What do you have to say for yourself now?\n\r";
unsigned char rx_buffer[RX_BUFFER_SIZE];
volatile enum {false, true} handshake;

uint8_t caractere;
uint8_t nouveauC;
void UART0_IRQHandler() {
  static uint32_t rx_char_counter = 0;
  unsigned char temp;
  nouveauC=1;


  temp = LPC_USART0->RXDAT ;
  rx_buffer[rx_char_counter] = temp;        // Append the current character to the rx_buffer
  WaitForUART0txRdy;                        // Wait for TXREADY
  LPC_USART0->TXDAT  = temp;                // Echo it back to the terminal

  if (temp == 0x0D) {                       // CR (carriage return) is current character. End of string.
    rx_buffer[rx_char_counter+1] = 0x0A;    // Append a new line character to rx_buffer.
    rx_buffer[rx_char_counter+2] = 0x00;    // Append a NUL terminator character to rx_buffer to complete the string.
    WaitForUART0txRdy;                      // Wait for TXREADY
    LPC_USART0->TXDAT  = 0x0A;              // Echo a NL (new line) character to the terminal.
    handshake = true;                       // Set handshake for main()
    rx_char_counter = 0;                    // Clear array index counter
    caractere = '\n';
  }
  else {                                    // Current character is not CR, keep collecting them.
    rx_char_counter++;                      // Increment array index counter.
    caractere = temp;
    if (rx_char_counter == RX_BUFFER_SIZE)  // If the string overruns the buffer, stop here before all hell breaks lose.
      while(1);
  }
  return;
}


int main(void) {
	int envois = 1;// c’est le même code pour les 2 puces
	char text[17];
	uint8_t ascending_number;
	uint8_t message_recue;
	uint8_t list = 'A';




	init_lcd();
	lcd_puts("TP ENS II1 2022");

	if (envois) { // objet TX


		nrf24_device(TRANSMITTER, RESET);
		while (1) {
			delay_function(TIME_GAP);
			//while(!nouveauC);
			//caractere = msg[ascending_number];
			//ascending_number = ascending_number % 13;
			while (nrf24_transmit(&list, 1, NO_ACK_MODE)
					== TRANSMIT_FAIL) //wait until payload is loaded into TX buffer
			{
				sprintf(text, "Transmit fail!");
				lcd_position(1, 0);
				lcd_puts(text);

			}
			while (nrf24_transmit_status() == TRANSMIT_IN_PROGRESS)
				;     //poll the transmit status, make sure the payload is sent

			sprintf(text, "Sent       "); //payload is sent without acknowledge successfully
			text[7] = list;
			list++;
			nouveauC = 0;
			lcd_position(1, 0);
			lcd_puts(text);
		}






	} else { // objet RX
		nrf24_device(RECEIVER, RESET);


		while (1) {
			//poll the transmit status, make sure the payload is sent

			while (nrf24_receive(&message_recue, 1) == RECEIVE_FIFO_EMPTY)
				;    //poll and receive in one simple function call


//			sprintf(text,"recue :     "); //payload is sent without acknowledge successfully
//			text[14] = message_recue;
			lcd_position(1, 8);
			lcd_putc(message_recue);

		}
	}

}

