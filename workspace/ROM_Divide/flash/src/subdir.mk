################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/MCUXpresso_Retarget.c \
../src/MCUXpresso_cr_startup.c \
../src/MCUXpresso_crp.c \
../src/MCUXpresso_mtb.c \
../src/ROM_Divide.c \
../src/Serial.c \
../src/rom_div_84x.c \
../src/system.c 

S_SRCS += \
../src/MCUXpresso_aeabi_romdiv_patch.s 

C_DEPS += \
./src/MCUXpresso_Retarget.d \
./src/MCUXpresso_cr_startup.d \
./src/MCUXpresso_crp.d \
./src/MCUXpresso_mtb.d \
./src/ROM_Divide.d \
./src/Serial.d \
./src/rom_div_84x.d \
./src/system.d 

OBJS += \
./src/MCUXpresso_Retarget.o \
./src/MCUXpresso_aeabi_romdiv_patch.o \
./src/MCUXpresso_cr_startup.o \
./src/MCUXpresso_crp.o \
./src/MCUXpresso_mtb.o \
./src/ROM_Divide.o \
./src/Serial.o \
./src/rom_div_84x.o \
./src/system.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCR_INTEGER_PRINTF -D __USE_ROMDIVIDE -D__USE_CMSIS -D__CODE_RED -DCORE_M0PLUS -D__MTB_DISABLE -D__MTB_BUFFER_SIZE=256 -D__REDLIB__ -I"/home/gabo/infoIndusLocal/workspace/ROM_Divide/inc" -I"/home/gabo/infoIndusLocal/workspace/peripherals_lib/inc" -I"/home/gabo/infoIndusLocal/workspace/utilities_lib/inc" -I"/home/gabo/infoIndusLocal/workspace/common/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m0 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.s src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -DDEBUG -D__CODE_RED -DCORE_M0PLUS -D__LPC84X__ -D__REDLIB__ -D__USE_ROMDIVIDE -g3 -mcpu=cortex-m0 -mthumb -D__REDLIB__ -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src

clean-src:
	-$(RM) ./src/MCUXpresso_Retarget.d ./src/MCUXpresso_Retarget.o ./src/MCUXpresso_aeabi_romdiv_patch.o ./src/MCUXpresso_cr_startup.d ./src/MCUXpresso_cr_startup.o ./src/MCUXpresso_crp.d ./src/MCUXpresso_crp.o ./src/MCUXpresso_mtb.d ./src/MCUXpresso_mtb.o ./src/ROM_Divide.d ./src/ROM_Divide.o ./src/Serial.d ./src/Serial.o ./src/rom_div_84x.d ./src/rom_div_84x.o ./src/system.d ./src/system.o

.PHONY: clean-src

