################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Base_TER.c \
../src/MCUXpresso_cr_startup.c \
../src/MCUXpresso_crp.c \
../src/MCUXpresso_mtb.c \
../src/PZEM004TNew.c \
../src/apcupsdcli.c \
../src/lib_ENS_II1_lcd_v2.c \
../src/system.c \
../src/voltmetreI2C.c 

C_DEPS += \
./src/Base_TER.d \
./src/MCUXpresso_cr_startup.d \
./src/MCUXpresso_crp.d \
./src/MCUXpresso_mtb.d \
./src/PZEM004TNew.d \
./src/apcupsdcli.d \
./src/lib_ENS_II1_lcd_v2.d \
./src/system.d \
./src/voltmetreI2C.d 

OBJS += \
./src/Base_TER.o \
./src/MCUXpresso_cr_startup.o \
./src/MCUXpresso_crp.o \
./src/MCUXpresso_mtb.o \
./src/PZEM004TNew.o \
./src/apcupsdcli.o \
./src/lib_ENS_II1_lcd_v2.o \
./src/system.o \
./src/voltmetreI2C.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCR_INTEGER_PRINTF -D__USE_CMSIS -D__CODE_RED -DCORE_M0PLUS -D__MTB_DISABLE -D__MTB_BUFFER_SIZE=256 -D__REDLIB__ -I"/home/gabo/travail_tmp/TER/workspace/Base_TER/inc" -I"/home/gabo/travail_tmp/TER/workspace/peripherals_lib/inc" -I"/home/gabo/travail_tmp/TER/workspace/utilities_lib/inc" -I"/home/gabo/travail_tmp/TER/workspace/common/inc" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fmerge-constants -fmacro-prefix-map="$(<D)/"= -mcpu=cortex-m0 -mthumb -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src

clean-src:
	-$(RM) ./src/Base_TER.d ./src/Base_TER.o ./src/MCUXpresso_cr_startup.d ./src/MCUXpresso_cr_startup.o ./src/MCUXpresso_crp.d ./src/MCUXpresso_crp.o ./src/MCUXpresso_mtb.d ./src/MCUXpresso_mtb.o ./src/PZEM004TNew.d ./src/PZEM004TNew.o ./src/apcupsdcli.d ./src/apcupsdcli.o ./src/lib_ENS_II1_lcd_v2.d ./src/lib_ENS_II1_lcd_v2.o ./src/system.d ./src/system.o ./src/voltmetreI2C.d ./src/voltmetreI2C.o

.PHONY: clean-src

