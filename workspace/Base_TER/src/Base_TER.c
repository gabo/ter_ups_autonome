// Programme de base TP II ENS

#include "lib_ENS_II1_lcd_v2.h"
#include "LPC8xx.h"
#include <stdio.h>
#include "syscon.h"
#include "i2c.h"
#include "swm.h"
#include "utilities.h"
#include "voltmetreI2C.h"
#include "PZEM004TNew.h"
#include "gpio.h"



// gestion de la communication avec le client sur la raspberry_pi
#include "apcupsdcli.h"

#define DBGTXPIN 4
#define DBGRXPIN 0

#define PinReseau     P0_13
#define PinStop       P0_21
#define PinChargeCtl  P0_11
#define PinRelais     P0_12

#define TENSIONLIMITRES 200
#define TENSIONLIMITBAT 11 // tension minimum de la batterie avant extinction forcée
#define TOPCHARGEBAT    14.5 * 1000
#define TOPCURRENT      1000 // valeurs à déterminé
#define COURANTFINDECHARGE 1000 * // dépend de la résistance de charge

#define relaisActive       0
#define relaisDesactive    1

uint16_t addressePZ = 0xF8;
char reseauDisponible = 1;

enum etat {fonctionnelle,defaillant,indisponible};

enum etat reseau = defaillant;
enum etat batterie = defaillant;

enum etat EtatDuReseau(void){ // renvois l'état du réseau
	//LPC_GPIO_PORT->B0[PinReseau] = 1;
	if(LPC_GPIO_PORT->B0[PinReseau]) return defaillant;
	return fonctionnelle;
};

void fermeRelais(){
	// attention logique inversé à plusieurs endroits dans le code
	LPC_GPIO_PORT->B0[PinRelais] = relaisActive;
};

void ouvreRelais(){
	// attention logique inversé à plusieurs endroits dans le code
	LPC_GPIO_PORT->B0[PinRelais] = relaisDesactive;
}

char rallumage = 0;

void PININT0_IRQHandler(void)
{ // Alerte réseau
	if(EtatDuReseau()==defaillant){
		fermeRelais();
	} else {
		rallumage = 1;
	};
	  // Clear any pending or left-over interrupt flags
	  LPC_PIN_INT->FALL = 1 << 0;
	  LPC_PIN_INT->RISE = 1 << 0;
}

//
// Pin Interrupt 1 ISR
//
void PININT1_IRQHandler(void)
{
	// Arrêt d'urgence, si le boutons est actionnées
	  if(LPC_GPIO_PORT->B0[PinStop]) ouvreRelais();

	  LPC_PIN_INT->FALL = 1 << 1;
	  LPC_PIN_INT->RISE = 1 << 1;
}

char stop(){
	if(LPC_GPIO_PORT->B0[PinStop]) return 0;
	return 1;
}


void EntrerSortieSetUp(){
	// Local variables
	  // Peripheral reset to the GPIO0 and pin interrupt modules. '0' asserts, '1' deasserts reset.
	  LPC_SYSCON->PRESETCTRL0 &=  (GPIO0_RST_N & GPIOINT_RST_N);
	  LPC_SYSCON->PRESETCTRL0 |= ~(GPIO0_RST_N & GPIOINT_RST_N);

	  // Enable clock to GPIO0 and pin interrupt modules.
	  LPC_SYSCON->SYSAHBCLKCTRL0 |= (GPIO0 | GPIO_INT);


	  // Configure P0.18, P0.19 as pin interrupts 1, 0
	 // Make PORT0.18, PORT0.19 outputs driving '0'.
	  LPC_GPIO_PORT->CLR0 = 0xC0000;           // Clear P0.18, P0.19 PIO0_13to '00'
	  LPC_GPIO_PORT->DIR0 |= 0xC0000 | (0<< PinStop) | (0 << PinReseau )|(1<<PinRelais);//|(1<<PinChargeCtl);          // P0.19, P0.20 to output*
	  uint32_t temporaire = LPC_IOCON->PIO0_13;
	  temporaire |= (0b1 << 10); // mode répéteur sur le pin
	  LPC_IOCON->PIO0_13 = temporaire;

	  // Configure P0.18 - P0.19 as pin interrupts 1 - 0 by writing to the PINTSELs in SYSCON

	  // Pin de surveillance du réseaux
	  LPC_SYSCON->PINTSEL[0] = PinReseau;  // PINTSEL0 is P0.18 simulation de chute du réseau
	  LPC_SYSCON->PINTSEL[1] = PinStop;  // PINTSEL1 is P0.19 Bouton évente
	  //LPC_SYSCON->PINTSEL[2] = ;

	  // Configure the Pin interrupt mode register (a.k.a ISEL) for edge-sensitive on PINTSEL1,0
	  LPC_PIN_INT->ISEL = 0x0;

	  // Configure the IENR (pin interrupt enable rising) for rising edges on PINTSEL0,1
	  LPC_PIN_INT->IENR = 0x3;

	  // Configure the IENF (pin interrupt enable falling) for falling edges on PINTSEL0,1
	  LPC_PIN_INT->IENF = 0x3;

	  // Clear any pending or left-over interrupt flags
	  LPC_PIN_INT->IST = 0xFF;

	  // Enable pin interrupts 0 - 1 in the NVIC (see core_cm0plus.h)
	  NVIC_EnableIRQ(PININT0_IRQn);
	  NVIC_EnableIRQ(PININT1_IRQn);
}


int TempoRemiseSousTension= 0;

int main(void) {
	// Activation du périphérique d'entrées/sorties TOR et interruption
	EntrerSortieSetUp(); // premier truc à set up, les entrés sorties
	//


	setPinRx(P0_16);
	setPinTx(P0_10);
	PZEM004T_initialisation();
	PZEM004T_setAddress(addressePZ);
	PZEM004T_updateValues();
	resetMillis();


	uint32_t tension_bat;
	int16_t courant_bat;
	uint32_t tensionDC[3];
	int16_t courantDC[3];
	uint32_t tension_res;
	int courant_res;
	uint32_t frequence_res;
	uint32_t temperature;
	enum etat StatutReseau;

	// link des pointeurs avec les variables de suivis
	// Les querys APC upsd seront ainsi toujours à jours
	apcsetupSerial(DBGTXPIN,DBGRXPIN);

	setOrigin_tension_bat(&tension_bat);
	setOrigin_tension_res(&tension_res);
	setOrigin_courant_res(&courant_res);
	setOrigin_frequence_res(&frequence_res);
	setOrigin_temperature_bat(&temperature);

	int i = 0;

	init_lcd();
	//lcd_puts("TP ENS II1 2022");

	while (1) {


		majTension(tensionDC);
		majCourant(courantDC);
		majTemperature(&temperature);
		tension_bat = tensionDC[0];




		for(int i = 0;i<50000;i++); // #TODO: utilisé un vrai timer plustot que de se résoudre à faire cela

		tension_res = PZEM004T_voltage();
		courant_res = PZEM004T_current();
		frequence_res = PZEM004T_frequency();

		if(rallumage & i>2){
			i =0;
			rallumage = 0;
			ouvreRelais();
		};

		if(rallumage) i++;


		PZEM004T_updateValues(); // mise à jours des valeurs
		apcmajdesvaleur();
		// dans un second temps il faudrait récupéré les valeurs via PZEM004T_recieve();
		for(int i = 0;i<500000;i++); // #TODO: utilisé un vrai timer plustot que de se résoudre à faire cela
		reseauDisponible = PZEM004T_recieve();



	} // end of while(1)

} // end of main
