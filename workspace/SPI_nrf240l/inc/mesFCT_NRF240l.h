#include "macroRegistre.h"

// À partir d’ici tout finis dans la librairie macroRegistre.h
void attend(){
	for(int j = 0; j < 10000; j++);
}
// On définit notre strucutre de travail
typedef struct {
	uint8_t status;
	uint32_t adresse;
	uint8_t taille_des_adresses;
	uint32_t RX_reception;
} NRF24_type_definition;

// Toute ce qui sera utilisé dans la bibliothèque
NRF24_type_definition NRF24; // instantiation de notre périphérique

void SPI_setup() {
	// Enable clocks to relevant peripherals
	LPC_SYSCON->SYSAHBCLKCTRL[0] |= (SPI0 | SWM);

	// Configure the SWM (see peripherals_lib and swm.h)
	ConfigSWM(SPI0_SCK, SCK_PIN);
	ConfigSWM(SPI0_MOSI, MOSI_PIN);
	ConfigSWM(SPI0_SSEL0, SSEL_PIN); // configuratio du chip select
	ConfigSWM(SPI0_MISO, MISO_PIN); // configuration d’un port MISO

	// Give SPI0 a reset
	LPC_SYSCON->PRESETCTRL[0] &= (SPI0_RST_N);
	LPC_SYSCON->PRESETCTRL[0] |= ~(SPI0_RST_N);

	// Enable main_clk as function clock to SPI
	LPC_SYSCON->SPI0CLKSEL = FCLKSEL_MAIN_CLK;

	// Get main_clk frequency
	SystemCoreClockUpdate();

	// Configure the SPI master's clock divider (value written to DIV divides by value+1)
	LPC_SPI0->DIV = (main_clk / SPIBAUD) - 1;

	// Configure the CFG register:
	// Enable=true, master, no LSB first, CPHA=0, CPOL=0, no loop-back, SSEL active low
	LPC_SPI0->CFG = SPI_CFG_ENABLE | SPI_CFG_MASTER;

	// Configure the SPI delay register (DLY)
	// Pre-delay = 0 clocks, post-delay = 0 clocks, frame-delay = 0 clocks, transfer-delay = 0 clocks
	LPC_SPI0->DLY = 0x0000;
	// Configure the SPI control register
	// Master: End-of-frame true, End-of-transfer true, RXIGNORE true, LEN 8 bits.
	LPC_SPI0->TXCTL = SPI_CTL_EOF | SPI_CTL_EOT | SPI_CTL_RXIGNORE
			| SPI_CTL_LEN(8);

	LPC_SPI0->TXCTL &= ~(0xF << 24);
	LPC_SPI0->TXCTL |= (0x7 << 24); // il faut écrire nb_bits - 1, 0 pour 1 bits, F pour 16 bit
}

int demande_status() {
	while (!(LPC_SPI0->STAT & SPI_TXRDY));
	//La mise à jours du status du périphérique devrait se
	// faire automatiquement avec les routines n’utilisant pas SPI_envoi().
	LPC_SPI0->TXCTL ^= (1 << 22); // au cas ou
	//LPC_SPI0->TXDAT = NRF24_STATUS;
	LPC_SPI0->TXDAT = NRF24_NOP; // ne fait rien mais permet de lire le status
	// permet de ne pas prendre de risque

	int i = 0;
	while (!(LPC_SPI0->STAT & 1)) {
		i++;
		if (i > 100000)
			return 0; // echec de l’envois d’informations, peut arrive en cas
		// déconnections
	}
	// sort de la boucle sur réception du flag de data reçus sur le ports du NRF24
	NRF24.status = LPC_SPI0->RXDAT;

	return 1;
}

int Ecrire_un_registre8bit(uint8_t adresse_registre, uint8_t contenue) {
	// pour écrire dans un registre il faut :
	// 1-écrire l’addresse du registre dans le premirer byte d’envois
	// 2-écrire les données dans les bytes suivant sans interruption de transmission
	// 3- mettre un terme à la transmission
	uint32_t Message_complet = (uint32_t) (contenue << 8)
			| (adresse_registre | NRF24_W_REGISTER);
	SPI_envoi(Message_complet, 2); // il à 2 bytes à envoyé

	//return demande_status(); // met à jours le status de la LPC
	return 0;
}

int Lire_un_registre8bit(uint8_t adresse_registre, uint8_t *buffer_de_lecture) {
	while (!(LPC_SPI0->STAT & SPI_TXRDY));

	LPC_SPI0->TXCTL &= ~(1 << 20);
	LPC_SPI0->TXCTL |= (1 << 21); // réactive the end of frame, dernier byte transférée
	LPC_SPI0->TXCTL &= ~(1 << 22); // on retire ici le rx ignore

	LPC_SPI0->TXDAT = adresse_registre + NRF24_R_REGISTER;
	int i = 0;
	while (!(LPC_SPI0->STAT & 1)) {
		i++;
		if (i > 100000)
			*buffer_de_lecture = 0xFF;
		return 0; // echec de l’envois d’informations, peut arrive en cas
		// déconnections
	}
	// sort de la boucle sur réce(adresse_registre | NRF24_W_REGISTER)ption du flag de data reçus sur le ports du NRF24
	NRF24.status = LPC_SPI0->RXDAT; // on met à jours le status

	while (!(LPC_SPI0->STAT & 2))
		; // attend que le port TX soit près à écrire

	LPC_SPI0->TXCTL |= (1 << 20);
	// On écrite un bytes, il ne doit pas être lu. Mais pendant ce temps le système
	LPC_SPI0->TXDAT = NRF24_NOP; // on envois un nope et pendants ce temps on reçois le status du r
	i = 0;
// on retire les modifications faites sur les registres de contrôle de la liaison SPI

	while (!(LPC_SPI0->STAT & 1)) {
		i++;
		if (i > 100000)
			*buffer_de_lecture = 0x2F;
		return 0;
	}
	*buffer_de_lecture = LPC_SPI0->RXDAT; // bonne lecture de la data

	return 1;
}

int Lire_un_registreNbyte(uint8_t adresse_registre, uint8_t Buffer_donnee[] , uint8_t nbByte) {
	// permet de lire un registre du plus de 8 bits
	while (!(LPC_SPI0->STAT & SPI_TXRDY));

	LPC_SPI0->TXCTL &= ~(1 << 20);
	LPC_SPI0->TXCTL |= (1 << 21); // désactive the end of trame, dernier byte transférée
	LPC_SPI0->TXCTL &= ~(1 << 22); // on retire ici le rx ignore
	// *(Buffer_donnee) = 0; // on vide le buffer
	LPC_SPI0->TXDAT = adresse_registre + NRF24_R_REGISTER; // on indique que l'on veut lire le registre
	int i = 0;


	while (!(LPC_SPI0->STAT & 1)) { // on attend ici qu’il y ai quelque chose en lecture
		i++;
		if (i > 100000)
			return 0; // echec de l’envois d’informations, peut arrive en cas
		// déconnections
	}

	NRF24.status = LPC_SPI0->RXDAT; // on met à jours le status

	int cptByte = 0; // compteur de byte

	while (cptByte < nbByte) { // tant que l'on à pas reçu tout les bytes

		if (cptByte == nbByte - 1) { // dernier bytes à transférés donc
			// on retire les modifications faites sur les registres de contrôle
			// c’est à dire la fin de séquence et la fin de volé de données, le système considèra alors le prochain byte
			// envoyé comme étant le dernier de la séquence d’envois
			LPC_SPI0->TXCTL |= (1 << 21); // fin de trame
			LPC_SPI0->TXCTL |= (1 << 20); // indique la fin de tranmission
		}

		while (!(LPC_SPI0->STAT & 2)); // attend que le port TX soit près à l’écriture;
		// On écrite un bytes, il ne doit pas être lu. Mais pendant ce temps le système

		LPC_SPI0->TXDAT = NRF24_NOP; // le NOP ne doit pas changer


		i = 0;
		while (!(LPC_SPI0->STAT & 1)) {
			i++;
			if (i > 100000)
				return 0;
		}

		//*(Buffer_donnee) |= (LPC_SPI0->RXDAT) << (cptByte*8); //on range les données dans notre buffer
		Buffer_donnee[cptByte] = LPC_SPI0->RXDAT;
		// de lecture de données
		cptByte++;

	}
	return 1; // fin de lecture de la série de données

}

int Ecrire_un_registreNbyte(uint8_t adresse_registre, uint8_t buffer_d_ecriture[], int nbByte){
	while (!(LPC_SPI0->STAT & SPI_TXRDY));
	LPC_SPI0->TXCTL &= ~(1 << 20);
	LPC_SPI0->TXCTL |= (1 << 21); // désactive the end of trame, dernier byte transférée
	LPC_SPI0->TXCTL &= ~(1 << 22); // on retire ici le rx ignore
	// *(Buffer_donnee) = 0; // on vide le buffer
	LPC_SPI0->TXDAT = adresse_registre | NRF24_W_REGISTER; // registre de commande
	int i = 0;

	while (!(LPC_SPI0->STAT & 1)) { // on attend ici qu’il y ai quelque chose en lecture
		i++;
		if (i > 100000)
			return 0; // echec de l’envois d’informations, peut arrive en cas
		// déconnections
	}
	LPC_SPI0->TXCTL |= (1 << 22); // maintenant on peut ignorer les données reçues
	NRF24.status = LPC_SPI0->RXDAT; // on met à jours le status

	int cptByte = 0; // compteur de byte

	while (cptByte < nbByte) { // tant que l'on à pas reçu tout les bytes

		if (cptByte == nbByte - 1) { // dernier bytes à transférés donc
			// on retire les modifications faites sur les registres de contrôle
			// c’est à dire la fin de séquence et la fin de volé de données, le système considèra alors le prochain byte
			// envoyé comme étant le dernier de la séquence d’envois
			LPC_SPI0->TXCTL |= (1 << 21);

		}

		while (!(LPC_SPI0->STAT & 2)); // attend que le port TX soit près à l’écriture;
		// On écrite un bytes, il ne doit pas être lu. Mais pendant ce temps le système

		LPC_SPI0->TXDAT = buffer_d_ecriture[cptByte]; // le NOP ne doit pas changer
		cptByte++;


	}
	LPC_SPI0->TXCTL |= (1 << 20); // indique la fin de tranmission
	return 1; // fin de lecture de la série de données
}

void SPI_envoi(uint32_t Data, int nbByte) {

	uint32_t sauvegarde = LPC_SPI0->TXCTL; // on enrigistre la configuration quelque part

	uint8_t buff; // on envois les donnnées bytes par bytes
	while (!(LPC_SPI0->STAT & SPI_TXRDY));
	LPC_SPI0->TXCTL |= (1 << 22);
	LPC_SPI0->TXCTL &= ~(1 << 20);
	LPC_SPI0->TXCTL &= ~(1 << 21); // désactive the end of trame, dernier byte transférée

	for (int i = 0; i < nbByte; ++i) {
		buff = (Data >> (i * 8));
		while (!(LPC_SPI0->STAT & SPI_TXRDY));
		LPC_SPI0->TXDAT = buff;
	}

	LPC_SPI0->TXCTL = sauvegarde; // on rétablie la configuration

}

