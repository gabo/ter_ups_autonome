#include "stdio.h"

void format(int *mesure_pt, char *nombre, int kst2conversion) {
  int mesure = mesure_pt[0];
  int entier = mesure / kst2conversion; // permet de récupéré la partie entière
  int dec = mesure - entier * kst2conversion; // on récup la version décimale
  sprintf(nombre, "%d.%02d", entier,
          dec); // à faire choisire en fonction de la forme de la
}

void copyString(char *des, char *orig, char nbcarac) {
  char i = 0;
  while (i < nbcarac) {
    des[i] = orig[i];
    i++;
  }
}

int main() {
  float nombre_ft = 1.41421;
  char racine22[9];
  int nombre = nombre_ft * 100000;
  format(&nombre, racine22, 100000);
  char test2[9];
  copyString(test2, racine22, 9);
  // printf(test2);
  printf(racine22);
}
