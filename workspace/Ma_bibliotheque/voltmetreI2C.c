/*
Bibliothèque pour lire la tension avec le module WCMCU-3221
++ lecture de la tension intégré à la carte NXP dans le module

*/

#include "voltmetreI2C.h"
#include "LPC8xx.h"
#include "i2c.h"
#include "swm.h"
#include "utilities.h"

// méthode pour lire l’i2c1 :

// l'adresse dépend de la configuration de la carte
// voir la documentation
unsigned char I2CMasterBuffer[tailleBuffer];
unsigned char I2CSlaveBuffer[tailleBuffer];
unsigned int I2CReadLength, I2CWriteLength;


#include <stdio.h>
#include "fro.h"
#include "rom_api.h"
#include "syscon.h"
#include "swm.h"
#include "i2c.h"

void init_i2c(void) {
	// inspirée du code init LCD
	// Provide main_clk as function clock to I2C0
	LPC_SYSCON->I2C0CLKSEL = FCLKSEL_MAIN_CLK;

	LPC_SYSCON->SYSAHBCLKCTRL0 |= I2C0 | SWM;

	//configuration des pins 7 (SDA) et 14 (SCL) en open_drain
	LPC_IOCON->PIO0_7 = 0x000004A0;
	LPC_IOCON->PIO0_14 = 0x000004A0;

	//assignation des pins 7 à SDA et 14 à SCL
	LPC_SWM->PINASSIGN5 = 0xFFFF0E07;

	// Give I2C0 a reset
	LPC_SYSCON->PRESETCTRL0 &= (I2C0_RST_N);
	LPC_SYSCON->PRESETCTRL0 |= ~(I2C0_RST_N);

	//horloge I2c
	LPC_I2C0->DIV = 1;
	LPC_I2C0->CFG = CFG_MSTENA;
}

unsigned int lireTension(const char RegTension) {
  unsigned int i;
  unsigned int valeurTension;

  // WaitI2CMasterState(LPC_I2C1, I2C_STAT_MSTST_IDLE); // Wait for the master
  // state to be idle
  while (!(LPC_I2C0->STAT & STAT_MSTPEND))
    ;
  /* clear buffer */
  for (i = 0; i < tailleBuffer; i++) {
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }

  unsigned char I2CMasterBuffer[tailleBuffer];
  unsigned char I2CSlaveBuffer[tailleBuffer];
  unsigned int I2CReadLength, I2CWriteLength;

  I2CWriteLength = 2;
  I2CReadLength = 0;
  I2CMasterBuffer[0] = Addr;
  I2CMasterBuffer[1] = RegTension;
  I2CMasterBuffer[2] = 0x00; /* configuration value, no change from default */
  I2CmasterWrite(I2CMasterBuffer, I2CWriteLength);

  /* clear buffer, then, get temp reading from LM75B */
  for (i = 0; i < tailleBuffer; i++) {
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }
  I2CWriteLength = 1;
  I2CReadLength = 2;
  I2CMasterBuffer[0] = Addr;
  I2CMasterBuffer[1] = RegTension;

  I2CmasterWriteRead(I2CMasterBuffer, I2CSlaveBuffer, I2CWriteLength,
                     I2CReadLength);

  valeurTension =
      (unsigned int)(((I2CSlaveBuffer[0] << 8) | I2CSlaveBuffer[1]));

  return valeurTension;
}



int16_t lireCourant(const char RegTension) {
  unsigned int i;
uint16_t valeurTension;

  // WaitI2CMasterState(LPC_I2C1, I2C_STAT_MSTST_IDLE); // Wait for the master
  // state to be idle
  while (!(LPC_I2C0->STAT & STAT_MSTPEND))
    ;
  /* clear buffer */
  for (i = 0; i < tailleBuffer; i++) {
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }

  unsigned char I2CMasterBuffer[tailleBuffer];
  unsigned char I2CSlaveBuffer[tailleBuffer];
  unsigned int I2CReadLength, I2CWriteLength;

  I2CWriteLength = 2;
  I2CReadLength = 0;
  I2CMasterBuffer[0] = Addr;
  I2CMasterBuffer[1] = RegTension;
  I2CMasterBuffer[2] = 0x00; /* configuration value, no change from default */
  I2CmasterWrite(I2CMasterBuffer, I2CWriteLength);

  /* clear buffer, then, get temp reading from LM75B */
  for (i = 0; i < tailleBuffer; i++) {
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }
  I2CWriteLength = 1;
  I2CReadLength = 2;
  I2CMasterBuffer[0] = Addr;
  I2CMasterBuffer[1] = RegTension;

  I2CmasterWriteRead(I2CMasterBuffer, I2CSlaveBuffer, I2CWriteLength,
                     I2CReadLength);

  valeurTension =
    (I2CSlaveBuffer[0] << 8) | (I2CSlaveBuffer[1] & (~0b111));
  //if(I2CSlaveBuffer[0] & (1<<8)) valeurTension = ~(valeurTension+1);
  //uint32_t valCourant = (uint32_t)(((I2CSlaveBuffer[0] << 8) | I2CSlaveBuffer[1]));

  int16_t valT = valeurTension;

  return valT;
}










#define I2CBAUD 		100000

/* For more info, read NXP's LM75B datasheet */
#define LM75_ADDR			0x90
#define LM75_CONFIG		0x01
#define LM75_TEMP			0x00

#define BUFSIZE       64


uint32_t I2CReadLength, I2CWriteLength;
uint32_t tempReading;

void setup_debug_uart(void);


uint32_t ReadTempSensor( void )
{
  uint32_t i;
	uint32_t tempValue;

  //WaitI2CMasterState(LPC_I2C0, I2C_STAT_MSTST_IDLE); // Wait for the master state to be idle
  while(!(LPC_I2C0->STAT & STAT_MSTPEND));
  /* clear buffer */
  for ( i = 0; i < BUFSIZE; i++ )	{
    I2CMasterBuffer[i] = 0;
		I2CSlaveBuffer[i] = 0;
  }

  /* the example used to test the I2C interface is
  a NXP's LM75 temp sensor. LPC MCU is used a I2C
  master, the temp sensor is a I2C slave. */

  /* the sequence to get the temp reading is:
  set configuration register,
  get temp reading
  */

  I2CWriteLength = 2;
  I2CReadLength = 0;
  I2CMasterBuffer[0] = LM75_ADDR;
  I2CMasterBuffer[1] = LM75_CONFIG;
  I2CMasterBuffer[2] = 0x00;		/* configuration value, no change from default */
  I2CmasterWrite( I2CMasterBuffer, I2CWriteLength );

  /* clear buffer, then, get temp reading from LM75B */
  for ( i = 0; i < BUFSIZE; i++ )	{
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }
  I2CWriteLength = 1;
  I2CReadLength = 2;
  I2CMasterBuffer[0] = LM75_ADDR;
  I2CMasterBuffer[1] = LM75_TEMP;
  I2CmasterWriteRead( I2CMasterBuffer, I2CSlaveBuffer, I2CWriteLength, I2CReadLength );
  /* The temp reading value should reside in I2CSlaveBuffer... */
	tempValue = (uint32_t)(((I2CSlaveBuffer[0]<<8) | I2CSlaveBuffer[1]) >> 5);
	/* D10 is the sign bit */
	if ( tempValue & 0x400 ) {
		/* after conversion, bit 16 is the sign bit */
		tempValue = (int32_t)(((-tempValue+1)&0x3FF) * 0.125) | 0x10000;
	}
	else {
		/* Bit 7 (D10) is the polarity, 0 is Plus temperature and 1 is Minus temperature */
		tempValue = (int32_t)(tempValue * 0.125);
	}
  return ( tempValue );
}


int lireTemperature(){

	  unsigned int i;
	  unsigned int valeurTemperature;

	  // WaitI2CMasterState(LPC_I2C1, I2C_STAT_MSTST_IDLE); // Wait for the master
	  // state to be idle
	  while (!(LPC_I2C0->STAT & STAT_MSTPEND))
	    ;
	  /* clear buffer */
	  for (i = 0; i < tailleBuffer; i++) {
	    I2CMasterBuffer[i] = 0;
	    I2CSlaveBuffer[i] = 0;
	  }

	  unsigned char I2CMasterBuffer[tailleBuffer];
	  unsigned char I2CSlaveBuffer[tailleBuffer];
	  unsigned int I2CReadLength, I2CWriteLength;

	  I2CWriteLength = 1;
	  I2CReadLength = 0;
	  I2CMasterBuffer[0] = Addr_thermometre << 1 | 1;
	  I2CMasterBuffer[1] = Reg_Temperature;
	  I2CMasterBuffer[2] = 0x00; /* configuration value, no change from default */
	  I2CmasterWrite(I2CMasterBuffer, I2CWriteLength);

	  /* clear buffer, then, get temp reading from LM75B */
	  for (i = 0; i < tailleBuffer; i++) {
	    I2CMasterBuffer[i] = 0;
	    I2CSlaveBuffer[i] = 0;
	  }
	  I2CWriteLength = 1;
	  I2CReadLength = 2;
	  I2CMasterBuffer[0] = Addr_thermometre << 1;
	  I2CMasterBuffer[1] = Reg_Temperature;

	  I2CmasterWriteRead(I2CMasterBuffer, I2CSlaveBuffer, I2CWriteLength,
	                     I2CReadLength);

	  valeurTemperature =
	      (unsigned int)(((I2CSlaveBuffer[0] << 8) | I2CSlaveBuffer[1])); // problème de conversion
	  valeurTemperature = valeurTemperature>>5;

	  valeurTemperature = (100*valeurTemperature)/8;
	  return valeurTemperature;// température en centième de degré celcius

}

void majTension(unsigned int *tensionEntree) {
  // lecture de toute les entrées en tensions
  tensionEntree[0] = lireTension(Reg_Tension1);
  tensionEntree[1] = lireTension(Reg_Tension2);
  tensionEntree[2] = lireTension(Reg_Tension3);
}

void majCourant(int16_t * courantEntree){
	// il faut lire les tensions
	  courantEntree[0] = lireCourant(Reg_Courant1);
	  courantEntree[1] = lireCourant(Reg_Courant2);
	  courantEntree[2] = lireCourant(Reg_Courant3);
}

void majTemperature(unsigned int * temperature){
	temperature[0] = ReadTempSensor();
}
