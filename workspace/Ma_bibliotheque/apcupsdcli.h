#ifndef _apc_
#define _apc_



#include "stdio.h"
#include "LPC8xx.h"
#include "fro.h"
#include "rom_api.h"
#include "swm.h"
#include "syscon.h"
#include "uart.h"
#include "utilities.h"
#include <stdio.h>






void setOrigin_tension_res(uint32_t * x);
void setOrigin_tension_bat(uint32_t * x);
void setOrigin_courant_bat(int* x);
void setOrigin_courant_res(uint32_t * x);
void setOrigin_frequence_res(uint32_t * x);
void setOrigin_temperature_bat(int * x);
//void defaultsetValue(void);
void testCommande(void);
void UART0_IRQHandler(void);
void apcsetupSerial(const char, const char);
void copyString(char *des, char *orig, char nbcarac);
void format(uint32_t*, char *, int);
void format_i(int*, char *, int);
void sentUART0(char);
char traiterCommande(char);
void urgence(void);
void apcmajdesvaleur(); // maj des valeurs
void checkAlertCond(void);



#endif // _apc_
