#ifndef PZEM004T_H
#define PZEM004T_H
#include "LPC8xx.h"
#include "fro.h"
#include "rom_api.h"
#include "stdint.h"
#include "swm.h"
#include "syscon.h"
#include "uart.h"
#include "utilities.h"
#include <stdio.h>

#define PZEM_DEFAULT_READ_TIMEOUT 1000
// implémentation du timer nécéssaire
#define PZEM_ERROR_VALUE -1
// l'ancienne valeur était -1.0 donc un flotant

#define true 1
#define false 0
#define NoData2transmit 0
#define Data2transmit 1

void PZEM004T_setReadTimeout(unsigned int msec);
void PZEM004T_initialisation();
unsigned int readTimeout(void); // code à remplacer

void setPinRx(const unsigned char pinRx);
void setPinTx(const unsigned char pinTx);

void initMillis(void);
unsigned int millis(void);
void resetMillis(void);

char PZEM004T_setAddress(uint8_t addr);
char PZEM004T_setPowerAlarm(uint16_t watt);
char PZEM004T_getPowerAlarm(void);
char PZEM004T_send(uint8_t cmd, uint16_t rAddr, uint16_t val, char check,
                   uint16_t slave_addr);
char PZEM004T_recieve(void);
void uartWrite(unsigned char *bytes, unsigned int size);

////////////// CRC externe

uint16_t CRC16(const uint8_t *data, uint16_t len);
char checkCRC(const uint8_t *buf, uint16_t len);
void setCRC(uint8_t *buf, uint16_t len);
char PZEM004T_updateValues(void);

// nouvelle version
uint32_t PZEM004T_voltage();
uint32_t PZEM004T_current();
uint32_t PZEM004T_power();
uint32_t PZEM004T_energy();
uint32_t PZEM004T_frequency();
uint32_t PZEM004T_pf();

#endif /*appel de la bibliothèque*/
