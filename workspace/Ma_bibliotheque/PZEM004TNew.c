
#include "PZEM004TNew.h"
#include <stdint.h>

#define REG_VOLTAGE 0x0000
#define REG_CURRENT_L 0x0001
#define REG_CURRENT_H 0X0002
#define REG_POWER_L 0x0003
#define REG_POWER_H 0x0004
#define REG_ENERGY_L 0x0005
#define REG_ENERGY_H 0x0006
#define REG_FREQUENCY 0x0007
#define REG_PF 0x0008
#define REG_ALARM 0x0009

#define CMD_RHR 0x03
#define CMD_RIR 0X04
#define CMD_WSR 0x06
#define CMD_CAL 0x41
#define CMD_REST 0x42

#define WREG_ALARM_THR 0x0001
#define WREG_ADDR 0x0002

#define UPDATE_TIME 200

#define RESPONSE_SIZE 32
#define READ_TIMEOUT 10000

#define INVALID_ADDRESS 0x00

#define PZEM_BAUD_RATE 9600
#define WaitForUART1txRdy while (((LPC_USART1->STAT) & (1 << 2)) == 0)

char newByte = false;
char caractere;
unsigned int _readTimeOut = PZEM_DEFAULT_READ_TIMEOUT;

unsigned char PZEM004T_pinRx;
unsigned char PZEM004T_pinTx;

uint8_t _addr = 0xF8; // Device address

char _isConnected; // Flag set on successful communication

struct {
  uint32_t voltage;
  uint32_t current;
  uint32_t power;
  uint32_t energy;
  uint32_t frequency;
  uint32_t pf;
  uint16_t alarms;
} _currentValues; // Measured values

uint64_t _lastRead; // Last time values were updated
uint8_t bufferRX[25];
uint8_t compteurRX = 0;
unsigned int readTimeout() { return _readTimeOut; } // code à remplacer

void PZEM004T_setReadTimeout(unsigned int msec) { _readTimeOut = msec; }

void resetMillis() {
  LPC_CTIMER0->TCR |= (1 << 2) + (1 << 0); // millis enable
  LPC_CTIMER0->TCR &= ~(1 << 2);
};
void initMillis() {
  /*
  Initialisation du timer CTIMER0; si il est utilisé, voir un autre timer
  */
  // set_fro_frequency(18000); // horloge à 9MHz
  fro_clk = 18000;
  LPC_CTIMER0->PR = 9000; // TC à 1kHz, compteur de milliseconde

  LPC_SYSCON->SYSAHBCLKCTRL0 |= CTIMER0;
  resetMillis();
};

unsigned int millis() {
  unsigned int timer = LPC_CTIMER0->TC;
  // cas de figure ou on proche de l'overflow du compteur
  return timer;
};

void setPinRx(const unsigned char pinRx) { PZEM004T_pinRx = pinRx; };
void setPinTx(const unsigned char pinTx) { PZEM004T_pinTx = pinTx; };

void PZEM004T_initialisation() {
  /*
   * Set up de la commmunication entre le PC et le micro controleur ( a
   * destination d'apcupsd)
   *
   * */

  LPC_SYSCON->SYSAHBCLKCTRL0 |= (SWM | UART1 | (1 << 15));
  LPC_SYSCON->FRG0MULT = 0;
  LPC_SYSCON->FRG0DIV = 255;
  // Select main_clk as the source for FRG0
  LPC_SYSCON->FRG0CLKSEL = FRGCLKSEL_MAIN_CLK;
  LPC_SYSCON->UART1CLKSEL = FCLKSEL_FRG0CLK;

  // Connect UART0 TXD, RXD signals to port pins
  ConfigSWM(U1_TXD,
            PZEM004T_pinTx); // configuration de l'uart pour les ports usb
  ConfigSWM(U1_RXD, PZEM004T_pinRx);

  LPC_SYSCON->PRESETCTRL0 &= (UART1_RST_N);
  LPC_SYSCON->PRESETCTRL0 |= ~(UART1_RST_N);
  LPC_USART1->BRG = 97; // 9800 bits/secondes
  LPC_USART1->CFG = DATA_LENG_8 | PARITY_NONE | STOP_BIT_1;
  LPC_USART1->CTL = 0;
  LPC_USART1->STAT = 0xFFFF;

  // Enable USART1
  LPC_USART1->CFG |= UART_EN;

  // Enable the USART1 RX Ready Interrupt
  LPC_USART1->INTENSET = RXRDY;
  NVIC_EnableIRQ(UART1_IRQn);
  initMillis();
}

void UART1_IRQHandler() {
  bufferRX[compteurRX++] = LPC_USART1->RXDAT; // accumulation de caractère
  if(bufferRX[0]!=_addr) {
	  compteurRX = 0;
	  return;
  };
  //if(compteurRX>=25)PZEM004T_recieve();
}

uint32_t PZEM004T_frequency(void) {

  return _currentValues.frequency;
}
uint32_t PZEM004T_voltage(void) {
  return _currentValues.voltage;
}

uint32_t PZEM004T_current(void) {
  return _currentValues.current;
}

uint32_t PZEM004T_power(void) {
  return _currentValues.power;
}

uint32_t PZEM004T_pf() {
  return _currentValues.pf;
}

char PZEM004T_resetEnergy() {
  uint8_t buffer[] = {0x00, CMD_REST, 0x00, 0x00};
  uint8_t reply[5];
  buffer[0] = _addr;

  setCRC(buffer, 4);
  uartWrite(buffer, 4);

  //uint16_t length = PZEM004T_recieve(reply, 5);

 // if (length == 0 || length == 5) {
 //   return false;
  //}

  return true;
}

uint32_t PZEM004T_energy(void) {
  return _currentValues.energy;
}

char PZEM004T_setAddress(uint8_t addr) {
  if (addr < 0x01 || addr > 0xF8) // sanity check
    return false;

  // Write the new address to the address register
  if (!PZEM004T_send(CMD_WSR, WREG_ADDR, addr, true, 0xF8))
    return false;

  _addr = addr; // If successful, update the current slave address

  return true;
}

char PZEM004T_setPowerAlarm(uint16_t watts) {
  if (watts > 25000) { // Sanitych check
    watts = 25000;
  }

  // Write the watts threshold to the Alarm register
  if (!PZEM004T_send(CMD_WSR, WREG_ALARM_THR, watts, true, _addr))
    return false;

  return true;
}

char PZEM004T_getPowerAlarm() {
  if (!PZEM004T_updateValues()) // Update vales if necessary
    return 0;                   // Update did not work, return 0

  return _currentValues.alarms != 0x0000;
}

char PZEM004T_updateValues() {
  // Read 10 registers starting at 0x00 (no check)
  PZEM004T_send(CMD_RIR, 0x00, 0x0A, false, _addr);
  return true;
}







char PZEM004T_send(uint8_t cmd, uint16_t rAddr, uint16_t val, char check,
                   uint16_t slave_addr) {

  uint8_t sendBuffer[8];
  //uint8_t respBuffer[8];

  if ((slave_addr == 0xFFFF) || (slave_addr < 0x01) || (slave_addr > 0xF8)) {
    slave_addr = _addr;
  }

  sendBuffer[0] = slave_addr; // Set slave address
  sendBuffer[1] = cmd;        // Set command

  sendBuffer[2] = (rAddr >> 8) & 0xFF; // Set high byte of register address
  sendBuffer[3] = (rAddr)&0xFF;        // Set low byte =//=

  sendBuffer[4] = (val >> 8) & 0xFF; // Set high byte of register value
  sendBuffer[5] = (val)&0xFF;        // Set low byte =//=

  setCRC(sendBuffer, 8); // Set CRC of fra
  uartWrite(sendBuffer, 8);

  return true;
}









void uartWrite(unsigned char *bytes, unsigned int size) {

  for (int i = 0; i < size; i++) {
    while (((LPC_USART1->STAT) & (1 << 2)) == 0) {
    };

    LPC_USART1->TXDAT = bytes[i];
  }
};

char PZEM004T_recieve() { // à commenté donc attendre
  if(bufferRX[0]!=_addr && !compteurRX) return false; // problème de faux positives

  if(compteurRX>=25){

  // Update the current values
  _currentValues.voltage = ((uint32_t)bufferRX[3] << 8 | // Raw voltage in 0.1V
                            (uint32_t)bufferRX[4]);

  _currentValues.current =
      ((uint32_t)bufferRX[5] << 8 | // Raw current in 0.001A
       (uint32_t)bufferRX[6] | (uint32_t)bufferRX[7] << 24 |
       (uint32_t)bufferRX[8] << 16);

  _currentValues.power =
      ((uint32_t)bufferRX[9] << 8 | // Raw power in 0.1W
       (uint32_t)bufferRX[10] | (uint32_t)bufferRX[11] << 24 |
       (uint32_t)bufferRX[12] << 16);

  _currentValues.energy =
      ((uint32_t)bufferRX[13] << 8 | // Raw Energy in 1Wh
       (uint32_t)bufferRX[14] | (uint32_t)bufferRX[15] << 24 |
       (uint32_t)bufferRX[16] << 16);

  _currentValues.frequency =
      ((uint32_t)bufferRX[17] << 8 | // Raw Frequency in 0.1Hz
       (uint32_t)bufferRX[18]);

  _currentValues.pf = ((uint32_t)bufferRX[19] << 8 | // Raw pf in 0.01
                       (uint32_t)bufferRX[20]);

  _currentValues.alarms = ((uint32_t)bufferRX[21] << 8 | // Raw alarm value
                           (uint32_t)bufferRX[22]);
  bufferRX[0] = 0;
  compteurRX = 0;
  return true;

  }


  return false;
}

//
// Pre computed CRC table
static const uint16_t crcTable[] = {
    0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241, 0XC601,
    0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440, 0XCC01, 0X0CC0,
    0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40, 0X0A00, 0XCAC1, 0XCB81,
    0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841, 0XD801, 0X18C0, 0X1980, 0XD941,
    0X1B00, 0XDBC1, 0XDA81, 0X1A40, 0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01,
    0X1DC0, 0X1C80, 0XDC41, 0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0,
    0X1680, 0XD641, 0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081,
    0X1040, 0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
    0X3600, 0XF6C1, 0XF881, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441, 0X3C00,
    0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41, 0XFA01, 0X3AC0,
    0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840, 0X2800, 0XE8C1, 0XE981,
    0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41, 0XEE01, 0X2EC0, 0X2F80, 0XEF41,
    0X2D00, 0XEDC1, 0XEC81, 0X2C40, 0XE401, 0X24C0, 0X2580, 0XE541, 0X2700,
    0XE7C1, 0XE681, 0X2640, 0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0,
    0X2080, 0XE041, 0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281,
    0X6240, 0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
    0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41, 0XAA01,
    0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840, 0X7800, 0XB8C1,
    0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41, 0XBE01, 0X7EC0, 0X7F80,
    0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40, 0XB401, 0X74C0, 0X7580, 0XB541,
    0X7700, 0XB7C1, 0XB681, 0X7640, 0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101,
    0X71C0, 0X7080, 0XB041, 0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0,
    0X5280, 0X9241, 0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481,
    0X5440, 0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
    0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841, 0X8801,
    0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40, 0X4E00, 0X8EC1,
    0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41, 0X4400, 0X84C1, 0X8581,
    0X4540, 0X8701, 0X47C0, 0X4680, 0X8641, 0X8201, 0X42C0, 0X4380, 0X8341,
    0X4100, 0X81C1, 0X8081, 0X4040};

uint16_t CRC16(const uint8_t *data, uint16_t len) {
  uint8_t nTemp;         // CRC table index
  uint16_t crc = 0xFFFF; // Default value

  while (len--) {
    nTemp = *data++ ^ crc;
    crc >>= 8;
    crc ^= (uint16_t)crcTable[nTemp];
  }
  return crc;
}

char checkCRC(const uint8_t *buf, uint16_t len) {
  if (len <= 2) // Sanity check
    return false;

  uint16_t crc = CRC16(buf, len - 2); // Compute CRC of data
  return ((uint16_t)buf[len - 2] | (uint16_t)buf[len - 1] << 8) == crc;
}

void setCRC(uint8_t *buf, uint16_t len) {
  if (len <= 2) // Sanity check
    return;

  uint16_t crc = CRC16(buf, len - 2); // CRC of data

  // Write high and low byte to last two positions
  buf[len - 2] = crc & 0xFF;        // Low byte first
  buf[len - 1] = (crc >> 8) & 0xFF; // High byte High
}
