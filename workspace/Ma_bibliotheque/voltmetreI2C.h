#ifndef moduleTensionI2C

#include "stdint.h"

#define moduleTensionI2C

//definitions pour I2C
#define I2CBAUD 		100000
#define LCD_ADDR		0x7C
#define BUFSIZE       	64

#define Reg_Courant1 0x01
#define Reg_Tension1 0x02
#define Reg_Courant2 0x03
#define Reg_Tension2 0x04
#define Reg_Courant3 0x05 // mesure de courant
#define Reg_Tension3 0x06

#define Addr 0b10000000 // on écriture car le dernier bit est 0
#define Addr_thermometre 0b1001000 // le premier bit est pour prendre le registre en lecture
#define Reg_Temperature 0

#define tailleBuffer 6

unsigned int lireTension(const char RegTension);
void majTension(unsigned int *tensionEntree);
void majCourant(int16_t * courantEntree);



/* For more info, read NXP's LM75B datasheet */
#define LM75_ADDR			0x90
#define LM75_CONFIG		0x01
#define LM75_TEMP			0x00



uint32_t ReadTempSensor(void);
#endif /*moduleTensionI2C*/
