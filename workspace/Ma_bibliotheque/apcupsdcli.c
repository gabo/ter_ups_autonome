
#include "apcupsdcli.h"
#include "stdint.h"

#define MODELEUPS "NXP-UPS 001"
#define TAILLE_REPONSE 50
#define TAILLE_MAX_NOMBRE 15
#define WaitForUART0txRdy  while(((LPC_USART0->STAT) & (1<<2)) == 0)
#define TENSION_NOMINAL_BATTERY_CHAR "13.7\n\r"

char ReponsePourApc[TAILLE_REPONSE]; // écriture
char *commandeInconnue = "NA\n\r";
int reception = 0;
unsigned char temp;

struct { // les valeurs sont formatées dedans
  char tension_bat[TAILLE_MAX_NOMBRE];
  char courant_bat[TAILLE_MAX_NOMBRE];
  char capacite[TAILLE_MAX_NOMBRE];
  char temperature[TAILLE_MAX_NOMBRE];
  char tension_res[TAILLE_MAX_NOMBRE];
  char courant_res[TAILLE_MAX_NOMBRE];
  char frequence_res[TAILLE_MAX_NOMBRE];
  char * statutUPS;
  char onBattery;
} statusUPS;

uint32_t * tension_res;
uint32_t * tension_bat;
int * courant_bat;
uint32_t * courant_res;
uint32_t * frequence_res;
int * temperature_bat;

void setOrigin_tension_res(uint32_t * x){
	tension_res =  x;
}

void setOrigin_tension_bat(uint32_t* x){
	tension_bat = x;
}

void setOrigin_courant_bat(int * x){
	courant_bat = x;
}

void setOrigin_courant_res(uint32_t* x){
	courant_res = x;
}

void setOrigin_frequence_res(uint32_t * x){
	frequence_res = x;
}

void setOrigin_temperature_bat(int * x){
	temperature_bat = x;
}


void apcmajdesvaleur(){


	// On met à jours dans la structure les grandeurs physiques via les pointeurs qui ont été mis à jours
	// dans la bibliothèque
	format(tension_res,statusUPS.tension_res,10);
	format(frequence_res,statusUPS.frequence_res,10);
	format(courant_res,statusUPS.courant_res,1000);




	// tension du voltmettre I2C arrive en millivolt
	format(tension_bat,statusUPS.tension_bat,1000);

	// température à implémenté
	format_i(temperature_bat,statusUPS.temperature,1);
	//format(999,statusUPS.tensio,10);
}



void testCommande(){
		traiterCommande(temp);
}

void UART0_IRQHandler() {
	reception = 1;
	//static uint32_t rx_char_counter = 0;
	temp = LPC_USART0->RXDAT;
	traiterCommande(temp);
}

void apcsetupSerial(const char _DBGTXPIN,const char _DBGRXPIN) {
	// Enable clocks to relevant peripherals
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (UART0 | SWM);

	// Connect UART0 TXD, RXD signals to port pins
	ConfigSWM(U0_TXD, _DBGTXPIN);
	ConfigSWM(U0_RXD, _DBGRXPIN);
	/*
	 * Communication PC
	 * */

	// Configure FRG0
	LPC_SYSCON->FRG0MULT = 0;
	LPC_SYSCON->FRG0DIV = 255;

	// Select main_clk as the source for FRG0
	LPC_SYSCON->FRG0CLKSEL = FRGCLKSEL_MAIN_CLK;

	// Select frg0clk as the source for fclk0 (to UART0)
	LPC_SYSCON->UART0CLKSEL = FCLKSEL_FRG0CLK;

	// Give USART0 a reset
	LPC_SYSCON->PRESETCTRL0 &= (UART0_RST_N);
	LPC_SYSCON->PRESETCTRL0 |= ~(UART0_RST_N);

	// Configure the USART0 baud rate generator
	LPC_USART0->BRG = 390; // dans l'ideal 389.625

	// Configure the USART0 CFG register:
	// 8 data bits, no parity, one stop bit, no flow control, asynchronous mode
	LPC_USART0->CFG = DATA_LENG_8 | PARITY_NONE | STOP_BIT_1;

	// Configure the USART0 CTL register (nothing to be done here)
	// No continuous break, no address detect, no Tx disable, no CC, no CLRCC
	LPC_USART0->CTL = 0;

	// Clear any pending flags, just in case
	LPC_USART0->STAT = 0xFFFF;

	// Enable USART0
	LPC_USART0->CFG |= UART_EN;

	// Enable the USART0 RX Ready Interrupt
	LPC_USART0->INTENSET = RXRDY;
	NVIC_EnableIRQ(UART0_IRQn);

}






void copyString(char *des, char *orig, char nbcarac) { // fonction de copy de chaine de char
	// code unsafe
  char i = 0;
  while (orig[i] && i < nbcarac) {
    orig[i] = des[i];
  	i++;
   }
}

void format(uint32_t * mesure_pt, char *nombre, int kst2conversion) {
  uint32_t mesure = mesure_pt[0];
  int entier = mesure / kst2conversion; // permet de récupéré la partie entière
  int dec = mesure - entier * kst2conversion; // on récup la version décimale
  sprintf(nombre, "%3d.%01d\n\r", entier,
          dec); // à faire choisire en fonction de la forme de la
}

void format_i(int * mesure_pt, char *nombre, int kst2conversion) {
	  int mesure = mesure_pt[0];
	  int entier = mesure / kst2conversion; // permet de récupéré la partie entière
	  int dec = mesure - entier * kst2conversion; // on récup la version décimale
	  sprintf(nombre, "%3d.%01d\n\r", entier,
	          dec); // à faire choisire en fonction de la forme de la
}

void sentUART0(char caractere) {
	WaitForUART0txRdy;
	LPC_USART0->TXDAT = caractere;
};

void transmissionAPC(char *reponse) {
  int i = 0; // pas besoin de le réinitialsié à chaque itération
  char c;
  while (reponse[i]){
	  	char c = reponse[i]; // chargement en attendant la libération du buffer TX
	  	i++;
		WaitForUART0txRdy;
		LPC_USART0->TXDAT = c;
  }
}

char traiterCommande(char caractere) {
  // traite les commandes, renvois
  // la valeur 0 si tout va bien, 1 sinon

	const char tension_nominal_bat[] = TENSION_NOMINAL_BATTERY_CHAR;
	const char Ack_connection[] = "SM\n\r";
	const char minTensionRes[] = "250.0\n\r";
	const char maxTensionRes[] = "200.0\n\r";



  switch (caractere) {

  // remplacer les caractères pas séquence qui marche
  case 0x01:
    // ^A
    // renvois le modèle de l'UPS : NXP-ENS 001
    transmissionAPC(MODELEUPS);
    return 0;
    break;

  case 'B':
    // renvois de la tension de la batterie
    transmissionAPC(statusUPS.tension_bat);
    return 0;
    break;

  case 'C':
    // renvois de la température interne de l'UPS
    transmissionAPC(statusUPS.temperature);
    return 0;
    break;

  case 'F':
    transmissionAPC(statusUPS.frequence_res);
    return 0;
    break;

  case 'K':

    break;
  case 'L':
	transmissionAPC(statusUPS.tension_res);
	return 0;
    break;
  case 'M':

    break;
  case 'N':
    // allumage
    // TODO:
    statusUPS.statutUPS = "On";
    return 0;

    break;
  case 'Q':
	sentUART0(0);
	sentUART0('\n');
	sentUART0('\r');
    return 0;
    break;

  case 'Y':
	transmissionAPC(Ack_connection);
	return 0;
	break;


  case 'g':
	transmissionAPC(tension_nominal_bat);
	return 0;
    break;
  };

  transmissionAPC(commandeInconnue);
  return 1; // problème
};

void checkAlertCond(void){
  /*

  Des messages urgences à envoyé lorsque que l'UPS est
  dans une condition anormale, passage sur batterie, batterie
  faible,etc. À voir dans la documentation dédié.

  ! = passage sur batterie
  $ = retour sur la ligne
  */

	//if(statusUPS.onBattery){
	//	sentUART0('%');
	//};


}
