/*
 ===============================================================================
 Name        : UART0_Terminal.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#include "LPC8xx.h"
#include "syscon.h"
#include "swm.h"
#include "syscon.h"
#include "utilities.h"
#include "uart.h"
#include "chip_setup.h"


#define RX_BUFFER_SIZE 35
#define WaitForUART0txRdy  while(((LPC_USART0->STAT) & (1<<2)) == 0)

/*

 message à envoyée
 Les messages envoyés au périphériques sont dans ce format :
 Slaves Address+0x04+Register Adress High Byte + Register address Low Byte
 + Number of Registers High Byte + Number of Registers Low Byte + CRC check High Byte + CRC check low Byte;

 On peut définir peu lire plusieur registre d'une traite
 Il est donc possible de lire la totalité des registre (et donc toute les mémoires);
 */
//const char RequeteCalibration[] = {1,6,0,1,8,0xFC,HH,LL};
//const char reglageAdresse[] = {6,};

const unsigned char thestring[] =
		"What do you have to say for yourself now?\n\r";
unsigned char rx_buffer[RX_BUFFER_SIZE]; //new line char (0xA) is appended to the array and echoed,
//                then a NUL char (0x0) is appended to the array to terminate the string
//                for future use.
// Parameters:    None
// Returns:       void
volatile enum {
	false, true
} handshake;

//
// Function name: UART0_IRQHandler
// Description:	  UART0 interrupt service routine.
//                This ISR reads one received char from the UART0 RXDAT register,
//                appends it to the rx_buffer array, and echos it back via the
//                UART0 transmitter. If the char. is 0xD (carriage return),
//                new line char (0xA) is appended to the array and echoed,
//                then a NUL char (0x0) is appended to the array to terminate the string
//                for future use.
// Parameters:    None
// Returns:       void
//
void UART0_IRQHandler() {
	static uint32_t rx_char_counter = 0;
	unsigned char temp;

	temp = LPC_USART0->RXDAT;
	rx_buffer[rx_char_counter] = temp; // Append the current character to the rx_buffer
	WaitForUART0txRdy ;
//	LPC_USART0->TXDAT = temp;
	//LPC_CTIMER0->TC;
	//LPC_CTIMER0->PR = 10000;



}





void initialisationUartUSB() {
	/*
	 * Set up de la commmunication entre le PC et le micro controleur ( a destination d'apcupsd)
	 *
	 * */
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (UART0 | SWM);

	// Connect UART0 TXD, RXD signals to port pins
	ConfigSWM(U0_TXD, DBGTXPIN); // configuration de l'uart pour les ports usb
	ConfigSWM(U0_RXD, DBGRXPIN);

	unsigned int tensionEntree[3];

	// Configure FRG0
	LPC_SYSCON->FRG0MULT = 0;
	LPC_SYSCON->FRG0DIV = 255;

	// Select main_clk as the source for FRG0
	LPC_SYSCON->FRG0CLKSEL = FRGCLKSEL_MAIN_CLK;

	LPC_SYSCON->UART0CLKSEL = FCLKSEL_FRG0CLK;

	// Give USART0 a reset
	LPC_SYSCON->PRESETCTRL0 &= (UART0_RST_N);
	LPC_SYSCON->PRESETCTRL0 |= ~(UART0_RST_N);
	LPC_USART0->BRG = 97;
	LPC_USART0->CFG = DATA_LENG_8 | PARITY_NONE | STOP_BIT_1;
	LPC_USART0->CTL = 0;
	LPC_USART0->STAT = 0xFFFF;

	// Enable USART0
	LPC_USART0->CFG |= UART_EN;

	// Enable the USART0 RX Ready Interrupt
	LPC_USART0->INTENSET = RXRDY;
	NVIC_EnableIRQ(UART0_IRQn);
}

//
// Main routine
//
int main(void) {
	initialisationUartUSB();

	unsigned char car = "a";
	unsigned int cycle = 0;
	int i = 0;

	while (1) {

		for (i = 0; i < 100000; i++)
			; // wait please
		LPC_USART0->TXDAT = car + cycle%25;
		WaitForUART0txRdy;
		LPC_USART0->TXDAT = ' ';
		WaitForUART0txRdy;
		LPC_USART0->TXDAT = '\r';
		cycle++;

	};

} // end of main

