// Programme de base TP II ENS

#include "lib_ENS_II1_lcd_v2.h"
#include "LPC8xx.h"
#include <stdio.h>
#include "syscon.h"
#include "i2c.h"
#include "swm.h"
#include "utilities.h"

#define Reg_Tension1 0x02
#define Addr 0b10000100  // on écriture car le dernier bit est 0




void I2CmasterWrite1( uint8_t *WrBuf, uint8_t WrLen )
{
	uint32_t i;

  while(!(LPC_I2C1->STAT & STAT_MSTPEND)); 		// Wait for the master state to be idle
  LPC_I2C1->MSTDAT = *WrBuf | 0;    					// Address with 0 for RWn bit (WRITE)
  LPC_I2C1->MSTCTL = CTL_MSTSTART;						// Start the transaction by setting the MSTSTART bit to 1 in the Master control register.
  while(!(LPC_I2C1->STAT & STAT_MSTPEND)); 			// Wait for the address to be ACK'd

	for ( i = 0; i < WrLen; i++ ) {
		LPC_I2C1->MSTDAT = *(WrBuf + i + 1);               // Send the data to the slave
		LPC_I2C1->MSTCTL = CTL_MSTCONTINUE;                // Continue the transaction
		while(!(LPC_I2C1->STAT & STAT_MSTPEND));   // Wait for the data to be ACK'd
  }
  LPC_I2C1->MSTCTL = CTL_MSTSTOP;                    // Send a stop to end the transaction
	return;
}


void I2CmasterWriteRead1( uint8_t *WrBuf, uint8_t *RdBuf, uint8_t WrLen, uint8_t RdLen )
{
	uint32_t i, i2c_addr;

	i2c_addr = *WrBuf;
  while(!(LPC_I2C1->STAT & STAT_MSTPEND));	// Wait for the master state to be idle
  LPC_I2C1->MSTDAT = i2c_addr;    										// Address with 0 for RWn bit (WRITE)
  LPC_I2C1->MSTCTL = CTL_MSTSTART;										// Start the transaction by setting the MSTSTART bit to 1 in the Master control register.
  while(!(LPC_I2C1->STAT & STAT_MSTPEND));	// Wait for the address to be ACK'd

	for ( i = 0; i < WrLen; i++ ) {
		LPC_I2C1->MSTDAT = *(WrBuf + i + 1);               // Send the data to the slave
		LPC_I2C1->MSTCTL = CTL_MSTCONTINUE;                // Continue the transaction
		while(!(LPC_I2C1->STAT & STAT_MSTPEND));   // Wait for the data to be ACK'd
  }

  LPC_I2C1->MSTDAT = i2c_addr | RD_BIT;    						// Address with 1 for RWn bit (READ)
  LPC_I2C1->MSTCTL = CTL_MSTSTART;										// Start the transaction by setting the MSTSTART bit to 1 in the Master control register.

	for ( i = 0; i < RdLen; i++ ) {
		while(!(LPC_I2C1->STAT & STAT_MSTPEND));		// Wait for the data to be ACK'd
		*(RdBuf + i) = LPC_I2C1->MSTDAT;									// Send the data to the slave
		LPC_I2C1->MSTCTL = CTL_MSTCONTINUE;								// Continue the transaction
  }
  LPC_I2C1->MSTCTL = CTL_MSTSTOP;                    // Send a stop to end the transaction
	return;
}



uint8_t I2CMasterBuffer[BUFSIZE];
uint8_t I2CSlaveBuffer[BUFSIZE];
uint32_t I2CReadLength, I2CWriteLength;
uint32_t tempReading;

uint32_t lireTension( void )
{
  uint32_t i;
	uint32_t tempValue;

  //WaitI2CMasterState(LPC_I2C1, I2C_STAT_MSTST_IDLE); // Wait for the master state to be idle
  while(!(LPC_I2C1->STAT & STAT_MSTPEND));
  /* clear buffer */
  for ( i = 0; i < BUFSIZE; i++ )	{
    I2CMasterBuffer[i] = 0;
		I2CSlaveBuffer[i] = 0;
  }


  /* the sequence to get the temp reading is:
  set configuration register,
  get temp reading
  */
  uint8_t I2CMasterBuffer[BUFSIZE];
  uint8_t I2CSlaveBuffer[BUFSIZE];
  uint32_t I2CReadLength, I2CWriteLength;
  uint32_t tempReading;
  I2CWriteLength = 2;
  I2CReadLength = 0;
  I2CMasterBuffer[0] = Addr;
  I2CMasterBuffer[1] = Reg_Tension1;
  I2CMasterBuffer[2] = 0x00;		/* configuration value, no change from default */
  I2CmasterWrite( I2CMasterBuffer, I2CWriteLength );

  /* clear buffer, then, get temp reading from LM75B */
  for ( i = 0; i < BUFSIZE; i++ )	{
    I2CMasterBuffer[i] = 0;
    I2CSlaveBuffer[i] = 0;
  }
  I2CWriteLength = 3;
  I2CReadLength = 2;
  I2CMasterBuffer[0]=Addr;
  I2CMasterBuffer[1] = Reg_Tension1;

  I2CmasterWriteRead( I2CMasterBuffer, I2CSlaveBuffer, I2CWriteLength, I2CReadLength );
  /* The temp reading value should reside in I2CSlaveBuffer... */
	tempValue = (uint32_t)(((I2CSlaveBuffer[0]<<8) | I2CSlaveBuffer[1]) >> 5);
	/* D10 is the sign bit */
	if ( tempValue & 0x400 ) {
		/* after conversion, bit 16 is the sign bit */
		tempValue = (int32_t)(((-tempValue+1)&0x3FF) * 0.125) | 0x10000;
	}
	else {
		/* Bit 7 (D10) is the polarity, 0 is Plus temperature and 1 is Minus temperature */
		tempValue = (int32_t)(tempValue * 0.125);
	}
  return ( tempValue );
}

void initI2C1(){
	  // Provide main_clk as function clock to I2C0
	  LPC_SYSCON->I2C1CLKSEL = FCLKSEL_MAIN_CLK;

	  // Enable bus clocks to I2C0, SWM
	  LPC_SYSCON->SYSAHBCLKCTRL0 |= (I2C1 | SWM);

		ConfigSWM(I2C1_SCL, P0_14 );               // Use for LPC804
	  ConfigSWM(I2C1_SDA, P0_7);               // Use for LPC804

	  // Give I2C0 a reset
	  LPC_SYSCON->PRESETCTRL0 &= (I2C0_RST_N);
	  LPC_SYSCON->PRESETCTRL0 |= ~(I2C0_RST_N);

	  // Configure the I2C0 clock divider
	  // Desired bit rate = Fscl = 100,000 Hz (1/Fscl = 10 us, 5 us low and 5 us high)
	  // Use default clock high and clock low times (= 2 clocks each)
	  // So 4 I2C_PCLKs = 100,000/second, or 1 I2C_PCLK = 400,000/second
	  // I2C_PCLK = SystemClock = 30,000,000/second, so we divide by 30000000/400000 = 75
	  // Remember, value written to DIV divides by value+1.
		// main_clk value is updated inside setup_debug_uart() already.
	  LPC_I2C0->DIV = (main_clk/(4*I2CBAUD)) - 1;

	  LPC_I2C0->CFG = CFG_MSTENA;
}



int main(void) {



	uint32_t i = 0;
	uint8_t bouton1;
	char text[17]={};

	// Activation du périphérique d'entrées/sorties TOR
	LPC_SYSCON->SYSAHBCLKCTRL0 |= GPIO;
	initI2C1();
	init_lcd();
	lcd_puts("TP ENS II1 2022");

	while (1) {


		sprintf(text,"%d V",lireTension());
		lcd_position(1,0);
		lcd_puts(text);

		//attente brève
		for (i = 0; i < 100000; i++);

	} // end of while(1)

} // end of main
